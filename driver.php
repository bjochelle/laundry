  <?php include "header.php";?>
  <style type="text/css">
      #list ul li{
            list-style: decimal;
            font-size: 20px;
      }
  </style>
    <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="assets/img/hero/banner.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>Be Our Partner</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Be our partner</a></li> 
                                        <li class="breadcrumb-item"><a href="#">Driver</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- ================ contact section start ================= -->
        <section class="contact-section" style="padding: 75px 0 75px;">
            <div class="container">
              
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">MOTORCYCLE REQUIREMENTS</h2>
                    </div>
                    <div class="col-lg-8">
                        <div class="row" style="margin-left: 40px;" id="list">
                            <ul>
                                <li>Motorcycle OR/CR *</li>
                                <li>Police Clearance *</li>
                                <li>Motorcycle must be in good condition with heat guard and no modifications have been made (year model 2013 above and 125cc to 155cc)</li>
                            </ul>
                           
                        </div>
                        <br>

                        <div class="col-12">
                            <h2 class="contact-title">BICYCLE REQUIREMENTS</h2>
                        </div>
                        <div class="col-lg-8">
                            <div class="row" style="margin-left: 40px;" id="list">
                                <ul>
                                    <li>Any government-issued valid ID *</li>
                                </ul>
                            </div>
                        </div>
                            <br>

                         <div class="col-12">
                            <h2 class="contact-title">VEHICLE REQUIREMENTS</h2>
                        </div>
                        <div class="col-lg-12">
                            <div class="row" style="margin-left: 40px;" id="list">
                                <ul>
                                    <li>LTFRB Application Form and Official Receipt with Valid and updated Temporary Authority Stamp OR Valid and updated Motion for PA Extension with Receipt and Temporary Authority Stamp OR valid Provisional Authority (For Expired PA)</li>

                                    <li>OR/CR or Sales Invoice and Delivery Receipt </li>
                                    <li>Vehicle Owner’s Valid Government-Issued ID with 3 specimen signature</li>
                                    <li>Comprehensive Insurance</li>
                                    <li>Picture of front and back of vehicle (conduction sticker and plate number is clearly visible </li>


                                </ul>
                            </div>
                            <br>
                        </div>
                         <h2 class="contact-title">DELIVERY-PARTNER REQUIREMENTS</h2>
                            <div class="row" style="margin-left: 40px;" id="list">
                            <ul>
                                <li>Professional Driver’s License (at least 21 days before expiration) *</li>
                                <li>NBI Clearance (at least 21 days before expiration) *</li>
                                <li>Age must be 18-55 yrs old ONLY Fit to work (51-55 yrs old above) *</li>
                                <li>Police Clearance or Barangay Clearance*</li>

                            </ul>
                             <span style="color:red;font-weight: bold;">NOTE: Sports motorcycle, Dirt Bike, and China brand motors are not allowed</span>
                            <span style="color:red;font-weight: bold;">*Mandatory Requirements</span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Negros Occidental,Philippines.</h3>
                                <p>Bacolod 6100</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>+639 253 565 2365</h3>
                                <p>Mon to Sun 8am to 10pm</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>support@laundry.com</h3>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ contact section end ================= -->
    </main>
  <?php include "footer.php";?>