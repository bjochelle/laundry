  <?php include "header.php";?>
    <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="assets/img/hero/banner.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>Login</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Login</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- ================ contact section start ================= -->
        <section class="contact-section" style="padding: 60px 0 60px;">
            <div class="container">
              
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Login</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" id="loginForm" novalidate="novalidate">
                            <div class="row">
                                
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="username" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your Username'" placeholder="Enter your Username">
                                    </div>
                                </div>

                                 <div class="col-sm-6">
                                 
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="password" id="name" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your Password'" placeholder="Enter your Password">
                                    </div>
                                </div>
                              
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Login</button>
                            </div>
                             <div class="form-group mt-3">
                               <a href="register.php" style="color: black;font-family: 'Teko',sans-serif;margin-top: 0px;font-style: normal;font-weight: 500;text-transform: normal;"> Not yet Register? Sign up here! </a>
                            </div>
                        </form>
                        <span id="notif" class="notif"> </span>

                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Negros Occidental,Philippines.</h3>
                                <p>Bacolod 6100</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>+639 253 565 2365</h3>
                                <p>Mon to Sun 8am to 10pm</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>support@laundry.com</h3>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ contact section end ================= -->
    </main>
  <?php include "footer.php";?>

  <script type="text/javascript">
      $(document).ready(function(){
        $("#loginForm").submit(function(e){
        e.preventDefault();
         $.ajax({
            url:"ajax/login_user.php",
            method:"POST",
            data:$(this).serialize(),
            success: function(data){
              if(data == 'home'){
                 $("#notif").html("<span style='color:green;'>Successfully Login</span>");
                setTimeout(function(){
                     window.location.replace("home/index.php?page="+data);
                },1000);
             }else{
                 $("#notif").html("<span style='color:orange;'>Incorrect Password or Username</span>");
              }
            }
          });
        })

      })
  </script>