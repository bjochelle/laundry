<?php include "header.php";?>
<style type="text/css">
    .notif{
        color:red;

    }
    .input-shadow{
        box-shadow: 0 0 15px #ff0000;
    }
</style>
    <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="assets/img/hero/banner.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>Register</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Register</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- ================ contact section start ================= -->
        <section class="contact-section">
            <div class="container">
              
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Register</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" id="registerForm" novalidate="novalidate">
                            <div class="row">
                                
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="fname" id="fname" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your First name'" placeholder="Enter your First name" >
                                        <span id="fname_msg" class="notif"></span>
                                    </div>
                                </div>

                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="lname" id="lname" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your Last name'" placeholder="Enter your Last name" >
                                        <span id="lname_msg" class="notif"></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="date" id="date" type="date" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your Birthdate'" placeholder="Enter your Birthdate" required>
                                        <span id="date_msg" class="notif"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="number" id="number" type="number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter phone number'" placeholder="Phone Number" >
                                        <span id="number_msg" class="notif"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="username" id="username" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Username'" placeholder="Username" >
                                        <span id="username_msg" class="notif"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="password" id="password" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Password'" placeholder="Password" >
                                        <span id="password_msg" class="notif"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" id='signup_btn' class="button button-contactForm boxed-btn">Sign Up</button>
                            </div>
                        </form>
                        <span id="notif">  </span>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Negros Occidental,Philippines.</h3>
                                <p>Bacolod 6100</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>+639 253 565 2365</h3>
                                <p>Mon to Sun 8am to 10pm</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>support@laundry.com</h3>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ contact section end ================= -->
    </main>
  <?php include "footer.php";?>
  <script type="text/javascript">
      $(document).ready(function(){
        $("#registerForm").submit(function(e){
            e.preventDefault();
            $(".notif").show();
            $(".notif").text("");
            var fname = $("#fname").val();
            var lname = $("#lname").val();
            var date = $("#date").val();
            var number = $("#number").val();
            var username = $("#username").val();
            var password = $("#password").val();
            $("#signup_btn").prop("disabled", true);
            $("#signup_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            if(fname == ""){
                setTimeout(function(){
                    $("#fname").removeClass("input-shadow");
                },2000);
                $("#fname").addClass("input-shadow");
            }
            if(lname == ""){
                setTimeout(function(){
                    $("#lname").removeClass("input-shadow");
                },2000);
                $("#lname").addClass("input-shadow");
               
            }
            if(date == ""){
                setTimeout(function(){
                    $("#date").removeClass("input-shadow");
                },2000);
                $("#date").addClass("input-shadow");
            }
            if(number == ""){
                setTimeout(function(){
                    $("#number").removeClass("input-shadow");
                },2000);
                $("#number").addClass("input-shadow");
            }
            if(username == ""){
                setTimeout(function(){
                    $("#username").removeClass("input-shadow");
                },2000);
                $("#username").addClass("input-shadow");
            }
            if(password == ""){
                setTimeout(function(){
                    $("#password").removeClass("input-shadow");
                },2000);
                $("#password").addClass("input-shadow");
            }

            // setTimeout(function(){
            //         $(".notif").hide();
            // },1000);

            if(fname != "" && lname != "" &&date !="" &&number !="" &&username !="" &&password !=""){
                 $.ajax({
                url:"ajax/register.php",
                method:"POST",
                data:$(this).serialize(),
                success: function(data){
                $("#signup_btn").prop("disabled", false);
                $("#signup_btn").html("Sign Up");
                  if(data == 1){
                    $.notify({
                        icon: "fa fa-check-circle",
                        title: "All Good!",
                        message: "Your data was successfully added"
                    },{
                        type: "success",
                        placement: {
                            from: "bottom",
                            align: "right"
                        }
                    })
                    setTimeout(function(){
                         window.location.replace("login.php");
                    },3000);
                 }else if(data == 2){
                    $.notify({
                        icon: "fa fa-exclamation-circle",
                        title: "Aw Snap!",
                        message: "Username Already Exist."
                    },{
                        type: "warning",
                        placement: {
                            from: "bottom",
                            align: "right"
                        }
                    })
                  }else{
                    $.notify({
                        icon: "fa fa-times-circle",
                        title: "Aw Snap!",
                        message: "Unable to execute your action, Please try again"
                    },{
                        type: "danger",
                        placement: {
                            from: "bottom",
                            align: "right"
                        }
                    })
                  }
                }

                
              });
            }else{
                $.notify({
                    icon: "fa fa-exclamation-circle",
                    title: "Aw Snap!",
                    message: "Please Fill all Required Fields."
                },{
                    type: "warning",
                    placement: {
                        from: "bottom",
                        align: "right"
                    }
                })
                $("#signup_btn").prop("disabled", false);
                $("#signup_btn").html("Sign Up");
            }


        })
      })
  </script>>