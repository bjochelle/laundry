-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 31, 2021 at 07:28 AM
-- Server version: 10.4.14-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u764488932_laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_driver`
--

CREATE TABLE `tbl_driver` (
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `motor_desc` text NOT NULL,
  `or_cr` varchar(225) NOT NULL,
  `police_clearance` varchar(225) NOT NULL,
  `drivers_license` varchar(225) NOT NULL,
  `nbi_clearance` varchar(225) NOT NULL,
  `filename` text NOT NULL,
  `fee` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_driver`
--

INSERT INTO `tbl_driver` (`driver_id`, `user_id`, `motor_desc`, `or_cr`, `police_clearance`, `drivers_license`, `nbi_clearance`, `filename`, `fee`) VALUES
(1, 14, '222', '222', '222', '222', '222', '', '50.00'),
(2, 24, 'test description s ', '12546 3 ', 'test-police s ', 'test-driver license s ', 'test-nbi s ', '', '50.00'),
(3, 27, 'qqqq', '111', 'qqq', 'qqqq', 'qqq', '', '50.00'),
(28, 29, 'eqwe', '213', '21312', '312', '312', '', '50.00'),
(29, 32, 'mio  ', '232  ', '44442  ', '44222  ', '432422  ', '', '50.00'),
(30, 35, 'Mio color yellow', '21', '456', '123', '567', '', '50.00'),
(31, 39, 'Mio', '1234', '038689', '1235890', '153736', '', '50.00'),
(32, 46, 'asasa', '12', 'sasa', 'asa', 'asasas', '', '60.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_merchant`
--

CREATE TABLE `tbl_merchant` (
  `merchant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `dti` varchar(225) NOT NULL,
  `bir` varchar(225) NOT NULL,
  `location_lat` varchar(50) NOT NULL,
  `location_long` varchar(50) NOT NULL,
  `tin` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_merchant`
--

INSERT INTO `tbl_merchant` (`merchant_id`, `user_id`, `name`, `address`, `contact_number`, `email`, `dti`, `bir`, `location_lat`, `location_long`, `tin`) VALUES
(2, 17, 'Luvada Laundry Services ', 'Alijis Rd, Bacolod, 6100 Negros Occidental  ', '0908235343  ', 'mercy@gmail.com', '324  ', '324  ', '10.6412338', '122.9467984', '324  ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE `tbl_rating` (
  `rating_id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` text NOT NULL,
  `driver_rating` int(11) NOT NULL,
  `driver_comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`rating_id`, `trans_id`, `service_id`, `rating`, `comment`, `driver_rating`, `driver_comment`, `date_added`, `user_id`) VALUES
(5, 5, 8, 3, 'test', 0, '', '2020-08-21 16:11:08', 1),
(6, 5, 8, 4, 'test', 0, '', '2020-08-21 16:18:08', 1),
(7, 3, 8, 3, 'm,mmmmmm', 0, '', '2020-08-23 14:46:00', 1),
(10, 16, 19, 5, 'test', 0, '', '2020-09-26 09:25:20', 26),
(11, 17, 20, 5, 'Excellent Service!!! very acommodating :)', 0, '', '2020-09-27 16:31:10', 34),
(12, 31, 18, 5, 'excellent', 0, '', '2020-10-23 13:38:39', 30),
(13, 13, 10, 3, 'test', 5, 'dsad', '2020-10-24 21:51:44', 1),
(14, 45, 10, 5, '', 5, '', '2021-01-30 16:27:01', 30);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE `tbl_services` (
  `service_id` int(11) NOT NULL,
  `service_type` varchar(225) NOT NULL,
  `category` text NOT NULL,
  `description` text NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `packaging` varchar(225) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`service_id`, `service_type`, `category`, `description`, `price`, `packaging`, `merchant_id`, `filename`) VALUES
(6, 'S', 'Comforter', 'complete package', '65.00', 'kilo', 2, '1606375290.png'),
(7, 'D', 'Dress', 'complete package', '30.00', 'kilo', 2, '1606375275.png'),
(8, 'B', 'Hand Wash', 'complete package', '100.00', 'kilo', 2, '1606375250.png'),
(9, 'B', 'Press/Iron', 'complete package', '45.00', 'kilo', 2, ''),
(10, 'D', 'Coat (Men)', 'complete package', '25.00', 'kilo', 2, '1606375224.png'),
(12, 'D', 'Others', 'complete package', '35.00', 'kilo', 2, '1606375238.png'),
(13, 'B', 'Press/Iron', 'complete package', '35.00', 'kilo', 2, '1606375202.png'),
(18, 'D', 'Blazer (Women)', 'complete package', '25.00', 'kilo', 2, '1606375190.png'),
(19, 'B', 'Wash Dry & Fold', 'complete package', '35.00', 'kilo', 2, '1606375167.png'),
(20, 'B', 'Wash Dry & Fold', 'complete package', '45.00', 'kilo', 2, '1606375154.png'),
(21, 'B', 'Wash Dry & Fold', 'Wash,Dry and Fold ', '35.00', 'kilo', 2, '1606375143.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_track_map_transaction`
--

CREATE TABLE `tbl_track_map_transaction` (
  `track_map_id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `point` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_track_map_transaction`
--

INSERT INTO `tbl_track_map_transaction` (`track_map_id`, `trans_id`, `latitude`, `longitude`, `point`) VALUES
(0, 41, '10.673400899999999', '122.93248619999999', 'O'),
(0, 41, '10.68399453697516', '122.95629799150865', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_track_transaction`
--

CREATE TABLE `tbl_track_transaction` (
  `track_id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `module` varchar(225) NOT NULL,
  `date_added` datetime NOT NULL,
  `read_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_track_transaction`
--

INSERT INTO `tbl_track_transaction` (`track_id`, `trans_id`, `module`, `date_added`, `read_status`) VALUES
(3, 4, 'PC', '2020-05-27 13:45:37', 1),
(10, 4, 'DL', '2020-05-27 14:33:33', 1),
(11, 4, 'PL', '2020-05-27 14:33:38', 1),
(14, 4, 'DC', '2020-05-27 14:36:14', 1),
(15, 1, 'PC', '2020-08-20 15:36:27', 1),
(16, 1, 'DL', '2020-08-20 15:36:40', 1),
(17, 1, 'PL', '2020-08-20 15:36:46', 1),
(18, 1, 'DC', '2020-08-20 15:36:49', 1),
(19, 3, 'PC', '2020-08-21 09:41:50', 1),
(20, 7, 'PC', '2020-08-21 09:44:43', 1),
(21, 7, 'DL', '2020-08-21 09:45:37', 1),
(22, 13, 'PC', '2020-08-21 13:52:29', 1),
(23, 3, 'DL', '2020-08-21 14:05:53', 1),
(24, 3, 'PL', '2020-08-21 14:06:07', 1),
(25, 3, 'DC', '2020-08-21 14:06:15', 1),
(26, 13, 'DL', '2020-08-22 18:03:19', 1),
(27, 13, 'PL', '2020-08-22 18:03:23', 1),
(29, 13, 'DC', '2020-08-22 18:05:02', 1),
(30, 14, 'PC', '2020-08-23 15:01:28', 1),
(31, 14, 'DL', '2020-08-23 15:01:30', 1),
(32, 14, 'PL', '2020-08-23 15:01:32', 1),
(33, 14, 'DC', '2020-08-23 15:01:34', 1),
(38, 16, 'DC', '2020-09-25 12:32:18', 1),
(39, 17, 'PC', '2020-09-27 16:22:49', 1),
(40, 17, 'DL', '2020-09-27 16:24:46', 1),
(41, 17, 'PL', '2020-09-27 16:26:49', 1),
(42, 17, 'DC', '2020-09-27 16:27:06', 1),
(43, 27, 'PC', '2020-10-22 15:15:56', 1),
(44, 27, 'DL', '2020-10-22 15:16:05', 1),
(45, 27, 'PL', '2020-10-22 15:16:13', 1),
(46, 27, 'DC', '2020-10-22 15:16:23', 1),
(47, 27, 'DC', '2020-10-22 15:16:25', 1),
(48, 27, 'DC', '2020-10-22 15:16:32', 1),
(49, 27, 'DC', '2020-10-22 15:16:45', 1),
(50, 27, 'DC', '2020-10-22 15:17:59', 1),
(51, 28, 'PC', '2020-10-22 15:27:13', 1),
(52, 29, 'PC', '2020-10-22 22:34:09', 1),
(53, 29, 'DL', '2020-10-22 22:38:45', 1),
(54, 29, 'PL', '2020-10-22 22:39:04', 1),
(55, 29, 'DC', '2020-10-22 22:39:14', 1),
(56, 29, 'DC', '2020-10-22 22:39:24', 1),
(57, 29, 'DC', '2020-10-22 22:39:25', 1),
(58, 19, 'PC', '2020-10-23 12:57:08', 0),
(59, 19, 'DL', '2020-10-23 12:57:22', 0),
(60, 19, 'PL', '2020-10-23 12:57:54', 0),
(61, 19, 'PL', '2020-10-23 12:57:55', 0),
(62, 31, 'PC', '2020-10-23 13:16:47', 1),
(63, 31, 'DL', '2020-10-23 13:18:33', 1),
(64, 31, 'PL', '2020-10-23 13:18:58', 1),
(65, 31, 'DC', '2020-10-23 13:20:28', 1),
(66, 31, 'DC', '2020-10-23 13:20:30', 1),
(67, 30, 'PC', '2020-10-24 21:56:38', 0),
(68, 30, 'PC', '2020-10-24 21:56:41', 0),
(69, 30, 'DL', '2020-10-24 21:56:59', 0),
(70, 30, 'PL', '2020-10-24 21:57:17', 0),
(71, 30, 'DC', '2020-10-24 21:57:25', 0),
(72, 19, 'DC', '2020-10-24 21:57:43', 0),
(73, 33, 'PC', '2020-10-28 15:49:58', 1),
(74, 33, 'DL', '2020-10-28 16:06:27', 1),
(75, 33, 'PL', '2020-10-28 16:09:23', 1),
(76, 33, 'DC', '2020-10-28 16:11:22', 1),
(77, 18, 'PC', '2020-11-09 10:40:45', 0),
(78, 18, 'DL', '2020-11-09 10:41:13', 0),
(79, 18, 'PL', '2020-11-09 10:41:36', 0),
(80, 18, 'DC', '2020-11-09 10:41:44', 0),
(81, 35, 'PC', '2020-11-09 23:27:59', 1),
(82, 35, 'DL', '2020-11-09 23:28:16', 1),
(83, 35, 'PL', '2020-11-09 23:28:25', 1),
(84, 37, 'PC', '2020-11-13 15:24:38', 1),
(85, 37, 'DL', '2020-11-13 15:24:54', 1),
(86, 40, 'PC', '2020-11-23 21:47:38', 1),
(87, 40, 'DL', '2020-11-23 21:47:48', 1),
(88, 40, 'PL', '2020-11-23 21:47:53', 1),
(89, 40, 'PL', '2020-11-23 21:48:13', 1),
(90, 40, 'PL', '2020-11-23 21:48:54', 1),
(91, 40, 'DC', '2020-11-23 21:50:34', 1),
(92, 37, 'PL', '2020-11-23 22:42:36', 1),
(93, 37, 'PL', '2020-11-23 22:42:38', 1),
(94, 37, 'PL', '2020-11-23 22:43:15', 1),
(95, 37, 'DC', '2020-11-23 22:43:21', 1),
(96, 37, 'DC', '2020-11-23 22:43:26', 1),
(97, 37, 'DC', '2020-11-23 22:43:28', 1),
(98, 41, 'PC', '2020-11-24 19:05:44', 1),
(99, 41, 'DL', '2020-11-24 19:05:50', 1),
(100, 41, 'DL', '2020-11-24 19:05:52', 1),
(101, 41, 'DL', '2020-11-24 19:06:03', 1),
(102, 41, 'PL', '2020-11-24 19:06:36', 1),
(103, 41, 'PL', '2020-11-24 19:06:37', 1),
(104, 41, 'PL', '2020-11-24 19:06:39', 1),
(105, 41, 'PL', '2020-11-24 19:06:51', 1),
(106, 41, 'PL', '2020-11-24 19:06:53', 1),
(107, 41, 'PL', '2020-11-24 19:07:19', 1),
(108, 41, 'DC', '2020-11-24 19:08:41', 1),
(109, 35, 'DC', '2021-01-29 20:01:58', 0),
(110, 42, 'PC', '2021-01-29 20:02:36', 0),
(111, 42, 'DL', '2021-01-29 20:02:49', 0),
(112, 42, 'PL', '2021-01-29 20:03:57', 0),
(113, 45, 'PC', '2021-01-30 16:21:22', 1),
(114, 45, 'DL', '2021-01-30 16:23:40', 1),
(115, 45, 'PL', '2021-01-30 16:23:58', 1),
(116, 45, 'DC', '2021-01-30 16:24:09', 1),
(117, 44, 'PC', '2021-01-30 16:32:08', 1),
(118, 44, 'DL', '2021-01-30 16:32:39', 1),
(119, 44, 'PL', '2021-01-30 16:34:11', 1),
(120, 44, 'DC', '2021-01-30 16:34:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `trans_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `note` text NOT NULL,
  `date_added` datetime NOT NULL,
  `sched_date` date NOT NULL,
  `sched_time` time NOT NULL,
  `sched_drop_off` datetime NOT NULL,
  `date_finish` datetime NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `qty` int(11) NOT NULL,
  `fee` decimal(12,2) NOT NULL,
  `pick_drop_lat` varchar(50) NOT NULL,
  `pick_drop_long` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`trans_id`, `user_id`, `merchant_id`, `service_id`, `driver_id`, `status`, `note`, `date_added`, `sched_date`, `sched_time`, `sched_drop_off`, `date_finish`, `price`, `qty`, `fee`, `pick_drop_lat`, `pick_drop_long`) VALUES
(1, 1, 2, 7, 14, 'F', 'sample', '2020-05-27 00:00:00', '0000-00-00', '00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.000', 10, '0.00', '', ''),
(3, 1, 2, 8, 24, 'F', '', '2020-08-22 00:00:00', '0000-00-00', '00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0.000', 13, '0.00', '', ''),
(4, 1, 2, 6, 14, 'F', 'sada', '2020-05-28 00:00:00', '2020-08-19', '08:00:00', '2020-08-22 10:00:00', '0000-00-00 00:00:00', '13.000', 12, '0.00', '', ''),
(5, 1, 2, 8, 24, 'F', 'note ', '2020-08-21 00:00:00', '0000-00-00', '00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '123.000', 1, '0.00', '', ''),
(6, 1, 2, 7, 14, 'C', '', '2020-08-20 00:00:00', '0000-00-00', '00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '3.000', 0, '0.00', '', ''),
(7, 1, 2, 7, 0, 'C', '14', '2020-08-21 00:00:00', '2020-08-22', '00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '3.000', 12, '0.00', '', ''),
(12, 1, 2, 10, 0, 'X', '', '2020-08-21 10:49:24', '2020-08-21', '11:48:00', '2020-08-21 12:00:00', '0000-00-00 00:00:00', '12.000', 0, '0.00', '', ''),
(13, 1, 2, 10, 24, 'F', '', '2020-08-21 13:51:27', '2020-08-22', '14:51:00', '2020-08-24 14:51:00', '2020-08-22 18:05:02', '12.000', 20, '0.00', '', ''),
(14, 1, 2, 10, 24, 'F', '', '2020-08-23 14:54:43', '2020-08-24', '14:54:00', '2020-08-25 14:54:00', '2020-08-23 15:01:34', '12.000', 12, '0.00', '', ''),
(15, 26, 2, 7, 27, 'C', 'dulong lng sa gate nga blue', '2020-09-25 12:05:25', '2020-09-25', '13:00:00', '2020-09-30 14:00:00', '0000-00-00 00:00:00', '3.000', 10, '0.00', '', ''),
(16, 26, 2, 19, 27, 'F', 'test remarks', '2020-09-25 12:29:35', '2020-09-25', '13:00:00', '2020-09-30 13:29:00', '2020-09-25 12:32:18', '35.000', 5, '0.00', '', ''),
(17, 34, 2, 20, 35, 'F', 'secure my clothes', '2020-09-26 15:15:43', '2020-09-27', '09:00:00', '2020-10-01 14:30:00', '2020-09-27 16:27:06', '100.000', 10, '50.00', '', ''),
(18, 35, 2, 20, 35, 'F', 'secure my clothes', '2020-09-26 15:16:41', '2020-09-27', '09:00:00', '2020-10-01 14:30:00', '2020-11-09 10:41:44', '100.000', 10, '50.00', '', ''),
(19, 35, 2, 8, 35, 'F', 'secure my clothes', '2020-09-26 15:19:20', '2020-09-26', '16:30:00', '2020-10-01 14:30:00', '2020-10-24 21:57:43', '100.000', 5, '50.00', '', ''),
(20, 34, 2, 20, 0, 'X', 'fsafasfasfasfafa', '2020-09-27 14:04:45', '2020-09-27', '16:03:00', '2020-09-29 10:04:00', '0000-00-00 00:00:00', '100.000', 0, '0.00', '', ''),
(21, 34, 2, 20, 2, 'C', 'pleawse secure my clothes not to damage', '2020-09-27 14:24:03', '2020-09-27', '16:25:00', '2020-09-29 10:30:00', '0000-00-00 00:00:00', '100.000', 0, '0.00', '', ''),
(22, 34, 2, 18, 0, 'X', 'assddsafsfafsfas', '2020-09-27 14:35:16', '2020-09-27', '14:36:00', '2020-09-27 14:39:00', '0000-00-00 00:00:00', '12.000', 0, '0.00', '', ''),
(23, 30, 2, 20, 0, 'X', 'banggaa lunok', '2020-10-21 16:43:41', '2020-10-22', '20:44:00', '2020-10-29 22:44:00', '0000-00-00 00:00:00', '100.000', 0, '0.00', '', ''),
(24, 30, 2, 7, 0, 'X', 'aw', '2020-10-21 16:48:02', '2020-10-23', '10:45:00', '2020-10-23 10:44:00', '0000-00-00 00:00:00', '3.000', 0, '0.00', '', ''),
(25, 1, 2, 7, 0, 'X', 'sda', '2020-10-21 18:08:05', '2020-10-22', '10:11:00', '2020-10-24 10:12:00', '0000-00-00 00:00:00', '3.000', 0, '0.00', '', ''),
(26, 30, 2, 10, 0, 'X', 'Awwww', '2020-10-21 18:27:32', '2020-10-22', '10:30:00', '2020-10-23 10:20:00', '0000-00-00 00:00:00', '12.000', 0, '0.00', '', ''),
(27, 30, 2, 13, 35, 'C', 'banggaa', '2020-10-22 15:12:23', '2020-10-23', '11:30:00', '2020-10-24 16:00:00', '0000-00-00 00:00:00', '123.000', 23, '50.00', '', ''),
(29, 40, 2, 9, 24, 'F', 'Bangga arao', '2020-10-22 22:14:47', '2020-10-23', '09:30:00', '2020-10-25 14:00:00', '2020-10-22 22:39:25', '213.000', 30, '0.00', '', ''),
(30, 40, 2, 7, 35, 'S', 'Bangga Abada', '2020-10-22 22:53:55', '2020-10-23', '10:30:00', '2020-10-24 11:00:00', '2020-10-24 21:57:25', '3.000', 0, '50.00', '', ''),
(31, 30, 2, 18, 35, 'F', 'bangga patyo', '2020-10-23 13:10:53', '2020-10-24', '09:00:00', '2020-10-26 08:00:00', '2020-10-23 13:20:30', '12.000', 5, '50.00', '', ''),
(32, 30, 2, 10, 0, 'X', 'Aaaa', '2020-10-24 21:54:36', '2020-10-25', '08:00:00', '2020-10-26 10:30:00', '0000-00-00 00:00:00', '12.000', 0, '0.00', '', ''),
(33, 43, 2, 7, 35, 'F', 'Yellow appartment', '2020-10-28 15:40:14', '2020-10-29', '09:30:00', '2020-10-30 10:00:00', '2020-10-28 16:11:22', '3.000', 5, '50.00', '', ''),
(34, 2, 2, 19, 0, 'S', 'bangga appart', '2020-10-28 16:01:52', '2020-10-30', '08:00:00', '2020-10-30 08:00:00', '0000-00-00 00:00:00', '35.000', 0, '0.00', '', ''),
(35, 1, 2, 7, 24, 'F', 'dsadas', '2020-11-09 23:26:15', '2020-11-10', '08:00:00', '2020-11-19 08:00:00', '2021-01-29 20:01:58', '3.000', 12, '0.00', '10.666584117494756', '122.93880899672241'),
(36, 30, 2, 21, 0, 'X', '', '2020-11-12 23:39:21', '2020-11-13', '09:30:00', '2020-11-15 09:30:00', '0000-00-00 00:00:00', '35.000', 0, '0.00', '10.6412338', '122.9467984'),
(37, 30, 2, 7, 35, 'F', '', '2020-11-12 23:41:07', '2020-11-13', '10:30:00', '2020-11-15 11:30:00', '2020-11-23 22:43:28', '3.000', 6, '50.00', '10.6412338', '122.9467984'),
(38, 30, 2, 7, 0, 'X', 'Aa', '2020-11-13 10:07:42', '2020-11-13', '10:30:00', '2020-11-15 09:30:00', '0000-00-00 00:00:00', '3.000', 0, '0.00', '10.6412338', '122.9467984'),
(39, 1, 2, 7, 35, 'C', '', '2020-11-23 21:32:13', '2020-11-25', '16:30:00', '2020-11-28 16:00:00', '0000-00-00 00:00:00', '3.000', 0, '50.00', '10.683827232141951', '122.95611634398651'),
(40, 44, 2, 7, 24, 'F', 'Bangga SSS', '2020-11-23 21:43:21', '2020-11-24', '08:00:00', '2020-11-28 13:30:00', '2020-11-23 21:50:34', '3.000', 12, '50.00', '10.683886696600307', '122.95628006602477'),
(41, 44, 2, 7, 24, 'F', 'Bangga ', '2020-11-23 21:50:29', '2020-11-24', '08:00:00', '2020-11-26 08:00:00', '2020-11-24 19:08:41', '3.000', 5, '50.00', '10.68399453697516', '122.95629799150865'),
(42, 24, 2, 7, 24, 'C', 'huhuhuhu', '2020-11-24 19:05:04', '2020-11-25', '08:00:00', '2020-11-26 10:00:00', '0000-00-00 00:00:00', '3.000', 12, '50.00', '10.677287386765546', '122.95075448534021'),
(43, 1, 0, 10, 0, 'X', '', '2020-12-01 08:40:35', '2020-12-01', '11:00:00', '2020-12-04 15:30:00', '0000-00-00 00:00:00', '25.000', 0, '0.00', '8.09592', '123.6846987'),
(44, 30, 2, 18, 35, 'F', 'Na', '2021-01-24 23:31:56', '2021-01-25', '08:00:00', '2021-01-26 10:30:00', '2021-01-30 16:34:16', '25.000', 4, '50.00', '10.6412338', '122.9467984'),
(45, 30, 2, 10, 35, 'F', 'aa', '2021-01-29 17:06:45', '2021-01-30', '10:30:00', '2021-02-01 15:30:00', '2021-01-30 16:24:09', '25.000', -3, '50.00', '10.6412338', '122.9467984');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(225) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `bday` date NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `un` varchar(225) NOT NULL,
  `pw` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `date_added` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  `ishidden` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `fname`, `lname`, `bday`, `contact_number`, `address`, `un`, `pw`, `status`, `date_added`, `email`, `filename`, `ishidden`) VALUES
(1, 'Jihyo', 'Park', '2020-05-22', '09107505919', 'tangub', 'q', 'q', 'C', '2020-05-22 00:00:00', 'jihyo@gmail.com', '1590469994.png', 0),
(2, 'Moiraaa', 'tan', '2020-05-22', '5325', '132', 'a', 'a', 'A', '2020-05-22 00:00:00', 'sdas@gmail.com', '1597973767.png', 1),
(3, 'asdas', 'dsad', '2020-05-27', '1412', '', 'das', 'dsa', 'C', '2020-05-22 00:00:00', '', '', 0),
(4, 'sadsa', 'das', '2020-05-01', '3423', '', 'sdas', 'dsa', 'C', '2020-05-22 00:00:00', '', '', 0),
(5, 'das', 'dasda', '2020-05-22', '414', '', '1241', 'q', 'C', '2020-05-22 18:58:27', '', '', 0),
(14, 'q', 'wq', '2020-05-01', '34321', 'rweqq', 'qq', '1234', 'D', '2020-05-26 10:22:46', 'sdaq@gmail.com', '1597909582.png', 0),
(17, 'John', 'Doe', '2020-05-26', '42343', 'rewr', 'jd', '12345', 'L', '2020-05-26 01:11:20', 'dasd@GMAIL.com', '1597909582.png', 0),
(18, 'q', 'qqq', '0000-00-00', '21321312 ', 'dasd ', 'l', '12345', 'L', '2020-05-26 01:12:34', '41234 ', '1590643326.png', 0),
(23, 'das', 'sad', '2020-06-08', '412412', 'sdas', 'dsad', '12345', 'L', '2020-06-08 14:44:32', 'dsadas@gmail.com', '', 0),
(24, 'Jiwon', 'Lee', '1990-07-01', '09500238475', 'Purok Roadstreet, Brgy.Tangub', 'jiwon_lee', '12345', 'D', '2020-08-21 09:26:45', 'jiwonlee@gmail.com', '1597973208.png', 0),
(25, 'sad', 'dsa', '2020-08-23', '124124', 'ddsadasd', 'sad', '12345', 'L', '2020-08-23 15:27:13', 'lomeranmarlyn@yahoo.com.ph', '', 0),
(26, 'sham', 'once', '1990-01-01', '09079566720', '', 'rose', 'rose', 'C', '2020-09-25 12:03:03', '', '', 0),
(27, 'moirene', 'sy', '1990-01-01', '09107505919', 'vista alegre', 'moirene', '12345', 'D', '2020-09-25 12:13:11', 'moiren@gmail.com', '1601007239.png', 0),
(28, 'John', 'Deer', '1990-01-01', '09454564469', 'Awing Bldg, Araneta Ave, Bacolod, 6100 Negros Occidental', 'John', '12345', 'L', '2020-09-25 12:18:41', 'john@gmail.com', '1601007540.png', 0),
(29, 'theo', 'lomeran', '1990-01-01', '09328912', 'brgy.35', 'theo', '12345', 'D', '2020-09-26 09:30:37', 'theo@gmail.com', '1601083857.png', 0),
(30, 'Rose', 'Baldespinosa', '2020-09-26', '09079566720', 'Alijis Rd, Bacolod, 6100 Negros Occidental', 'rose13', '123456', 'C', '2020-09-26 12:06:16', 'baldespinosarose13@gmail.com', '1601097846.png', 0),
(31, 'Ross', 'dyy', '2020-09-26', '0908235343', 'Bacolod, 6100 Negros Occidental Carlos Hilado Memorial State College alijis', 'Ross', '12345', 'L', '2020-09-26 13:30:36', 'baldespinosar@gmail.com', '', 0),
(32, 'Jomuel', 'edeza', '2020-09-26', '09232545533', 'Alijis Rd, Bacolod, 6100 Negros Occidental', '2', '12345', 'D', '2020-09-26 13:32:52', 'dyom@gmail.com', '', 0),
(33, 'Rein', 'Baldes', '2020-09-26', '093012344523', '', 'ren23', '12345', 'C', '2020-09-26 14:27:12', '', '', 0),
(34, 'Rose', 'Dado', '2020-09-26', '09079566720', '', 'rose22', 'baldespinosa13', 'C', '2020-09-26 14:45:39', '', '', 0),
(35, 'joms', 'edeza', '2020-09-26', '09112626832', 'Alijis Rd, Bacolod, 6100 Negros Occidental', 'dyom_22', 'baby22', 'D', '2020-09-26 14:49:28', 'dyomss@gmail.com', '', 0),
(36, 'Mercy', 'dads', '2020-09-26', '12345678901', 'BACOLOD MEMORIAL PARK, Alijis Road, Bacolod, Negros Occidental', 'Mercy', '12345', 'L', '2020-09-26 14:54:13', 'mercy@gmail.com', '', 0),
(37, 'Mercidita', 'aws', '2020-09-26', '1234567811901', 'Circumferencial Road, Bacolod, Negros Occidental', 'Mercidita', '12345', 'L', '2020-09-26 14:59:51', 'mercy@gmail.com', '', 0),
(38, 'Rosalinda', 'Dadooo', '2020-09-27', '09079566720', '', 'ros123', 'ros123', 'C', '2020-09-27 11:29:49', '', '', 0),
(39, 'Juan', 'Oy', '1991-09-27', '09001122334', 'Purok Paghidaet Alijis Bacolod City', 'Juan22', '12345', 'D', '2020-09-27 11:38:28', 'juan@gmail.com', '', 0),
(40, 'Maii', 'Young', '2020-10-22', '09484238370', 'Prk. Progreso Village phase 2A Brgy. Vista Alegre ', 'Jm', '08279800', 'C', '2020-10-22 22:07:04', '', '1603382909.png', 0),
(41, 'Kawyne', 'Gee', '2020-10-22', '09484238370', '', 'ryry', '01', 'C', '2020-10-22 22:58:50', '', '', 0),
(42, 'Rein', 'Borda', '1991-10-28', '09079566720', '', 'rein23', 'rein23', 'C', '2020-10-28 02:27:20', '', '', 0),
(43, 'jezreel', 'chu', '1990-06-28', '09461924092', '6th Street, Purok Pag-asa, Barangay 17, Bacolod City, Negros Occidental', 'jez123', '12345', 'C', '2020-10-28 15:31:45', 'alvaradojezreel56@gmail.com', '', 0),
(44, 'Em', 'Em', '2020-08-27', '09484238370', 'Prk. Progresso Village , brgy. Vista Alegre', 'em', 'me', 'C', '2020-11-16 10:25:58', 'emem@gmail.com', '', 0),
(45, '', '', '0000-00-00', '', '', '', '12345', 'L', '2020-11-18 22:37:05', '', '', 0),
(46, 'Chung', 'Chang', '2021-01-04', '09123456789', 'Bangga Abada', 'emem', '12345', 'D', '2021-01-30 14:51:07', 'emem@gmail.com', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_driver`
--
ALTER TABLE `tbl_driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `tbl_merchant`
--
ALTER TABLE `tbl_merchant`
  ADD PRIMARY KEY (`merchant_id`);

--
-- Indexes for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `tbl_track_transaction`
--
ALTER TABLE `tbl_track_transaction`
  ADD PRIMARY KEY (`track_id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_driver`
--
ALTER TABLE `tbl_driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_merchant`
--
ALTER TABLE `tbl_merchant`
  MODIFY `merchant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_track_transaction`
--
ALTER TABLE `tbl_track_transaction`
  MODIFY `track_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
