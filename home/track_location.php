<?php 
$id = $_GET['transid'];

?>
<style type="text/css">
	#track_map {
	    height: 450px;;  /* The height is 400 pixels */
	    width: 100%;  /* The width is the width of the web page */
	   }
</style>
<div class="content" style="width: 100%;">
  <div class="container-fluid">
      <div class="row">
          <div class='col-md-12'>
          	<input type="hidden" id='transID' value="<?=$id?>" name="">
            <div id="track_map"></div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    	setInterval( function(){
    		getLocation();
    	}, 10000);
    	
    	
    })
    
    function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition);
	    } else { 
	        $("#track_map").html("Geolocation is not supported by this browser.");
	    }
	}
	function showPosition(position) {
	  var transID = $("#transID").val();
	  // var lat = parseFloat(position.coords.latitude);
	  // var long = parseFloat(position.coords.longitude);
	  // alert(lat);
	  $.post("../ajax/savetrackinginfo.php",{
	    transID:transID,
	    latitude:position.coords.latitude,
	    longitude:position.coords.longitude
	  },function(data,status){
	  //	alert(data);
	      // var res = JSON.parse(data);
	      // var lat = parseFloat(position.coords.latitude);
	      // var long = parseFloat(position.coords.longitude);
	      loadMap(transID, position.coords.latitude, position.coords.longitude);
	  
	  });
	}
	function loadMap(transID, lat, long){
		$.post("../ajax/locations_tracking.php",{
			transID: transID
		}, function(data){
			var markers = JSON.parse(data);
			var map = new google.maps.Map(document.getElementById('track_map'), {
		        zoom:16,
		        center: {lat: lat, lng: long},
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		      });

			for (i = 0; i < markers.data.length; i++) {
				var marker = new google.maps.Marker({
			        position: new google.maps.LatLng(markers.data[i].lat, markers.data[i].long),
			        //icon: icon,
			        map: map,
			        // animation : google.maps.Animation.DROP,
			        label: markers.data[i].text
			      });

				// google.maps.event.addListener(marker, 'click', (function(marker, i) {
			 //        return function() {
			 //          infowindow.setContent(markers.data[i].text);
			 //          infowindow.open(map, marker);
			 //        }
			 //      })(marker, i));
			}
			 marker.setMap(map);
		});
	}
   
  </script>
  <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=getLocation"> </script>