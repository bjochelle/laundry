<?php 
include "../core/config.php";
  $status = $_SESSION['status'];
?>
<style type="text/css">
  h2{
    color:#3859bb;
    text-align: center;
  }
</style>
<div class="content" style="width: 100%;">
  <div class="container-fluid">
      <div class="row">
          <div class='col-md-12'>
            <div id="calendar"></div>
          </div>
      </div>
  </div>
</div>

<?php 
$query_getEvents = mysql_query("SELECT * FROM tbl_transaction WHERE driver_id = '$id'"); 
$count_events = mysql_num_rows($query_getEvents);
?>

<script type="text/javascript">
$(document).ready( function(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      events: [
        <?php 
          
        $ctrEvent=1;
        while($fetch_events = mysql_fetch_array($query_getEvents)){
            
          $start_date = $fetch_events['sched_date'];
          $end_date = date("Y-m-d", strtotime('+1 day', strtotime($fetch_events['sched_date'])));
          $event_time = date("g:ia", strtotime($fetch_events['sched_time']));
          $userid = $fetch_events['user_id'];
          $merchantid = $fetch_events['merchant_id'];
          $srvcid = $fetch_events['service_id'];

          $getname = mysql_fetch_array(mysql_query("SELECT CONCAT(fname,' ',lname) as `name`, `address` as `address` FROM tbl_user WHERE `user_id` = '$userid'"));

          $getMerch = mysql_fetch_array(mysql_query("SELECT `name`, `address` FROM tbl_merchant WHERE `merchant_id` = '$merchantid'"));

          $getSrvc = mysql_fetch_array(mysql_query("SELECT service_type, category, price, packaging FROM tbl_services WHERE `service_id` = '$srvcid'"));
          $srvctype = ($getSrvc[0] == 'B')?"Basic Items":(($getSrvc[0] == 'D')?"Dry Cleaning":"Special Items");
          $prc = $getSrvc[2]." / ".$getSrvc[3];
        ?>
        {
          title: '<?php echo $getname['name']." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          end: '<?php echo $end_date?>',
          color: '#3498db',
          id: '<?php echo $fetch_events['trans_id']; ?>',
          sched_date: '<?php echo $fetch_events['sched_date']; ?>',
          sched_time: '<?php echo $fetch_events['sched_time']; ?>',
          merchant: '<?php echo $getMerch[0]; ?>',
          address: '<?php echo $getMerch[1]; ?>',
          custName: '<?php echo $getname['name']; ?>',
          cusAddress: '<?php echo $getname['address']; ?>',
          srvc: '<?php echo $srvctype; ?>',
          cat: '<?php echo $getSrvc[1]; ?>',
          price: '<?php echo $prc; ?>'
        }<?php 
          if($ctrEvent < $count_events){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
        
      ],
      eventClick: function(callEvent , jsEvent, view){
        $("#viewDetailsBooking").modal();
        $("#merch_name").html(callEvent.merchant);
        $("#merch_address").html(callEvent.address);
        $("#cusName").html(callEvent.custName);
        $("#cusadd").html(callEvent.cusAddress);
        $("#srvctyp").html(callEvent.srvc);
        $("#cat").html(callEvent.cat);
        $("#prc").html(callEvent.price);
      }
    });
});
</script>
