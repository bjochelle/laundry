
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of Booking 
             <!--  <button type="submit" class="btn btn-info btn-fill pull-right" style="float: right" data-toggle="modal" data-target="#myModal1">Add</button> -->
        </h4>
        </div>
        <div class="card-body table-full-width table-responsive">

            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                    <th>Customer Name</th>
                    <th>Customer Address</th>
                   <!--  <th>Merchant</th>
                    <th>Merchant Address</th> -->
                    <th>Service Type</th>
                    <th>Note</th>
                    <th>Price</th>
                    <th>Date and Time</th>
                    <th>Action</th>

                </tr></thead>
                <tbody>
                </tbody>
            </table>
          
        </div>
    </div>
</div>
<script type="text/javascript">
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "url":"../ajax/datatables/dt_trans.php",
          "dataSrc":"data"
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"customer"
          },
          {
            "data":"cust_addr"
          },
          // {
          //   "data":"merchant"
          // },
          // {
          //   "data":"merch_addr"
          // },
          {
            "data":"service"
          },
          {
            "data":"note"
          },
          {
            "data":"price"
          }, 
          {
            "data":"date"
          },         
          {
            "mRender": function(data,type,row){
              return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Confirm' value='"+row.id+ "' onclick='confirm("+row.id+")'><span class='fa fa-check'></span>Confirm</button></center>";
            }
          }
        ]
      });
    }

 
$(document).ready(function (){
  getData();
});

 function confirm(id){
      $("#confirmModal").modal("show");
      $('#btn_confirm_booking').attr('onClick', 'confirmBooking('+id+')');

}

 function confirmBooking(id){

   $.ajax({
      url:"../ajax/confirmDriverBooking.php",
      method:"POST",
      data:{
        id:id
      },
      success: function(data){
      if(data == 1){
        alert_custom("fa fa-check-circle","All Good!","Booking was successfully confirmed","success");
        $("#confirmModal").modal("hide");
        getData();
      }else{
        failed_query();
      }
    }
  })
}
</script>