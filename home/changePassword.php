<?php

    include "../core/config.php";
    $id = $_SESSION['id'];
?>

<div class="content" style="width: 100%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Change Passwrod </h4>
                    </div>

                    <div class="card-body">
                        <form id="changePasswordForm">
                            <input type="hidden" id="user_id" name="user_id" value="<?php echo $id;?>">
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="form-group">
                                        <label>Current Password</label>
                                    <input type="password" class="form-control" placeholder="Current Password" name="curr_pw" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input type="password" class="form-control" name="new_pw"  id="new_pw"  placeholder="New Password" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Re-enter Password</label>
                                        <input type="password" class="form-control" id="re_pw"  placeholder="Re-enter Password" name="re_pw" required="">
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <button type="submit" class="btn btn-info btn-fill pull-right">Change Password</button>
                            <div class="clearfix"></div>
                        </form>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
     
        $("#changePasswordForm").submit(function(e){
        e.preventDefault();
        var re_pw = $("#re_pw").val();
        var new_pw = $("#new_pw").val();

            if(re_pw == new_pw ){
                 $.ajax({
                    url:"../ajax/update_password.php",
                    method:"POST",
                    data:$(this).serialize(),
                    success: function(data){
                        if(data == 1){
                             success_update();
                             window.location.replace("index.php?page=profile");
                            // location.reload();
                        }else if(data==2){
                            alert_custom("fa fa-times-circle","Aw Snap!","Password not matched","warning");
                        }else{
                            failed_query();
                        }
                    }
                  });
            }else{
                alert_custom("fa fa-exclamation-circle","Aw Snap!","Re-enter Password doesn't match to new Password","warning");
            }
        })

    })
</script>