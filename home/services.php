<style type="text/css">
  .form-control{
    border: 1px solid #495057;
  }
  .modal-dialog{
     margin: 0 auto;
  }
  .modal-content{
        top: 20px;
  }
    .cr-slider-wrap{
        display: none;
    }
</style>
 <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/croppie.min.css">
<!--   <script src="../assets/js/jquery.min.js"></script> -->
  <script src="../assets/js/croppie.js"></script>
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of Services 
              <button type="submit" class="btn btn-info btn-fill pull-right" style="float: right" data-toggle="modal" data-target="#myModalServices">Add</button>
        </h4>
        </div>
        <div class="card-body table-full-width table-responsive">

            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                    <th>Service Type</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Action</th>

                </tr></thead>
                <tbody>

                </tbody>
            </table>



        </div>
    </div>
</div>
 
<script type="text/javascript">
    $(document).ready(function(){
        $('#select_image').on('click', function() {
            $('#image_select').trigger('click');
        });

        $('#image_select').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize1.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });

          var resize1 = $('#upload_services_image').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 150,
                height: 150,
                type: 'square' //square
            },
            boundary: {
                width: 150,
                height: 150
            }
        });

         $('#update_service_img').on('click', function() {
            $('#image_select_update').trigger('click');
        });

        $('#image_select_update').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });

          var resize = $('#update_service_img_wrap').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 150,
                height: 150,
                type: 'square' //square
            },
            boundary: {
                width: 150,
                height: 150
            }
        });

        $('.btn-upload-services').on('click', function (ev) {
          var serv = $("#serv").val();
          var cat = $("#serv_category").val();
          var serv_desc = $("#serv_desc").val();
          var price = $("#price").val();
          var packaging = $("#packaging").val();
          resize1.croppie('result', {
            type: 'canvas',
            size:  { width: 300, height: 300 }
          }).then(function (img) {
            $.ajax({
              url: "../ajax/addServices.php",
              type: "POST",
              data: {
                "image":img,
                "serv":serv,
                "cat":cat,
                "serv_desc":serv_desc,
                "price":price,
                "packaging": packaging
              },
              success: function (data) {
                if(data == 1){
                  $("#myModalServices").modal("hide");
                    getData();
                    success_add();
                }else{
                  failed_query();
                }

              }
            });
          });
        });

         $('.btn-upload-services-update').on('click', function (ev) {
          ev.preventDefault();
          var id= $("#id").val();
          var serv = $("#serv1").val();
          var cat = $("#cat1").val();
          var serv_desc = $("#description").val();
          var price = $("#price_update").val();
          var image_val = $("#image_select_update").val();

          resize.croppie('result', {
            type: 'canvas',
            size: { width: 300, height: 300 }
          }).then(function (img) {

            $.ajax({
              url: "../ajax/updateServices.php",
              type: "POST",
              data: {
                "image_val":image_val,
                "image":img,
                "serv":serv,
                "cat":cat,
                "serv_desc":serv_desc,
                "price":price,
                "id":id
              },
              success: function (data) {
                if(data == 1){
                  $("#editModalServices").modal("hide");
                    getData();
                    success_update();
                }else{
                  failed_query();
                }

              }
            });
          });
        });


        // $('#image_select').on('change', function () { 
        //   var reader2 = new FileReader();
        //     reader2.onload = function (e) {
        //       resize2.croppie('bind',{
        //         url: e.target.result
        //       }).then(function(){
        //         console.log('jQuery bind complete');
        //       });
        //     }
        //     reader2.readAsDataURL(this.files[0]);
        // });

        // var resize2 = $('#update_service_img_wrap').croppie({
        //     enableExif: true,
        //     enableOrientation: true,    
        //     viewport: { // Default { width: 100, height: 100, type: 'square' } 
        //         width: 150,
        //         height: 150,
        //         type: 'square' //square
        //     },
        //     boundary: {
        //         width: 150,
        //         height: 150
        //     }
        // });

      });
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      var status = 'L';
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "url":"../ajax/datatables/dt_services.php",
          "dataSrc":"data"
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"service_type"
          },
          {
            "data":"category"
          },
          {
            "data":"price"
          },          
          {
            "mRender": function(data,type,row){
              return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Edit Details' value='"+row.id+ "' onclick='editDetails("+row.id+")'><span class='fa fa-edit'></span></button><button class='btn btn-danger btn-sm' data-toggle='tooltip' title='Delete' value='"+row.id+ "' onclick='dlDetails("+row.id+")'><span class='fa fa-trash'></span></button></center>";
            }
          }
        ]
      });
    }

    function getCategory(){
      var  serv = $("#serv").val();

      if(serv == 'B'){
          $(".cat").html('<option value="">Select type</option>'+
            '<option>Hand Wash</option>'+
            '<option>Press/Iron</option>'+
            '<option>Wash Dry & Fold</option>');
      }else if(serv == 'D'){
          $(".cat").html('<option value="">Select type</option>'+
            '<option>Blazer (Women)</option>'+
            '<option>Coat (Men)</option>'+
            '<option>Dress</option>'+
            '<option>Others</option>'+
            '<option>Polo</option>');
      }else if(serv == 'S'){
          $(".cat").html('<option value="">Select type</option>'+
            '<option>Bags</option>'+
            '<option>Barong</option>'+
            '<option>Blankets</option>'+
            '<option>Comforter</option>'+
            '<option>Jackets</option>'+
            '<option>Pillows</option>'+
            '<option>Shoes/Slippers/Caps</option>'+
            '<option>Stufed Toys</option>');
      }else{
          $("#cat").html('<option value=""></option>');

      }
    }

    function getCategory1(){
      var  serv = $("#serv1").val();
      if(serv == 'B'){
          $("#cat1").html('<option value="">Select type</option>'+
            '<option>Hand Wash</option>'+
            '<option>Press/Iron</option>'+
            '<option>Wash Dry & Fold</option>');
      }else if(serv == 'D'){
          $("#cat1").html('<option value="">Select type</option>'+
            '<option>Blazer (Women)</option>'+
            '<option>Coat (Men)</option>'+
            '<option>Dress</option>'+
            '<option>Others</option>'+
            '<option>Polo</option>');
      }else if(serv == 'S'){
          $("#cat1").html('<option value="">Select type</option>'+
            '<option>Bags</option>'+
            '<option>Barong</option>'+
            '<option>Blankets</option>'+
            '<option>Comforter</option>'+
            '<option>Jackets</option>'+
            '<option>Pillows</option>'+
            '<option>Shoes/Slippers/Caps</option>'+
            '<option>Stufed Toys</option>');
      }else{
          $("#cat1").html('<option value=""></option>');

      }
    }

$(document).ready(function (){
  getData();
});

function editDetails(id){
  $("#editModalServices").modal("show");
  $("#id").val(id);
  $.ajax({
      url:"../ajax/getServices.php",
      type:"POST",
      data:{
          id:id
        }
      ,success:function(data){
        var o = JSON.parse(data);
        $("#serv1").val(o.service_type);
        getCategory1();
        $("#merchant_id").val(o.merchant_id);
        $("#cat1").val(o.category);
        $("#description").val(o.description);
        $("#price_update").val(o.price);
        $("#packaging").val(o.packaging)
        
      }
  });
}

 $("#addServicesForm").submit(function(e){
    e.preventDefault();
    var merchant_select = $("#merchant_select").val();

    if(merchant_select == ''){
        alert("Please select merchant");
    }else{
        $.ajax({
          url:"../ajax/addServices.php",
          method:"POST",
          data:$(this).serialize(),
          success: function(data){
          if(data == 1){
            $("#myModalServices").modal("hide");
            $("#addServicesForm")[0].reset();
              getData();
              success_add();
          }else{
            failed_query();
          }
          }
        });
    }
    
    })

 // $("#editServicesForm").submit(function(e){
 //    e.preventDefault();
 //     $.ajax({
 //        url:"../ajax/updateServices.php",
 //        method:"POST",
 //        data:$(this).serialize(),
 //        success: function(data){
 //        if(data == 1){
 //          $("#editModalServices").modal("hide");
 //            getData();
 //            success_update();
 //        }else{
 //          failed_query();
 //        }
 //        }
 //      });
 //    })

 function dlDetails(id){
  $.ajax({
      url:"../ajax/deleteService.php",
      type:"POST",
      data:{
          id:id
        }
      ,success:function(data){
        if(data == 1){
            success_delete();
            getData();
        }else if(data == 2){
          alert_custom("fa fa-exclamation-circle","Aw Snap!","Unable to delete. Found important data","warning");
        }else{
          failed_query();
        }

      }
  });
 }
</script>