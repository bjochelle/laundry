<style type="text/css">
  .form-control{
    border: 1px solid #495057;
  }
  .modal-dialog{
     margin: 0 auto;
  }
  .modal-content{
        top: -100px;
  }
</style>
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of Delivery 
        </h4>
        </div>
        <div class="card-body table-full-width table-responsive">

            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                    <th>Customer Name</th>
                    <th>Merchant</th>
                    <th>Status</th>
                    <th>Date Finish</th>
                   <!--  <th>Action</th> -->
                </tr></thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      var status = 'L';
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "url":"../ajax/datatables/dt_delivery.php",
          "dataSrc":"data"
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"customer"
          },
          {
            "data":"merchant"
          },
          {
            "data":"status"
          },
          {
            "data":"date_finish"
          }
          // ,          
          // {
          //   "mRender": function(data,type,row){
          //     return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.id+ "' onclick='viewDetails("+row.id+")'><span class='fa fa-eye'></span></button></center>";
          //   }
          // }
        ]
      });
    }

  

$(document).ready(function (){
  getData();
});

 $("#addForm").submit(function(e){
    e.preventDefault();
     $.ajax({
        url:"../ajax/addServices.php",
        method:"POST",
        data:$(this).serialize(),
        success: function(data){
        if(data == 1){
          success_add();
          $("#myModal1").modal("hide");
          $("#addForm")[0].reset();
        }else{
          failed_query();
        }
        }
      });
})
</script>