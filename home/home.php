<?php 
include "../core/config.php";

$status = $_SESSION['status'];
if($status == 'D'){
    $stat = "Driver";
}else{
    $stat = "Laundry Merchant";
}


function getNotif($id,$remarks){
  $count_notif = mysql_query("SELECT * from tbl_track_transaction where trans_id='$id' and module='$remarks'");

  if($remarks == 'DL'){
    $task_number = 1;
  }else if($remarks == 'PL'){
    $task_number = 2;
  }else if($remarks == 'DC'){
    $task_number = 3;
  }else{
    $task_number = 0;
  }

  if(mysql_num_rows($count_notif) == 0){
    $function='onclick="deliverLaundry('.$id.','.$task_number.')" style="cursor: help;"';
  }else{
    $function='';
  }
  return $function;
}

function getButton($id,$remarks){
  $count_notif = mysql_query("SELECT * from tbl_track_transaction where trans_id='$id' and module='$remarks'");

  if($remarks == 'DL'){
    $task_number = 1;
  }else if($remarks == 'PL'){
    $task_number = 2;
  }else if($remarks == 'DC'){
    $task_number = 3;
  }else{
    $task_number = 0;
  }


  if(mysql_num_rows($count_notif) == 0){
    $function="<button class='btn btn-success'>";
  }else{
    $function="<span class='fa fa-check'></span>";
  }
  return $function;
}

function hideShow_map($id){
  $count_notif = mysql_query("SELECT * from tbl_track_transaction where trans_id='$id'");

  // if($remarks == 'DL'){
  //   $task_number = 1;
  // }else if($remarks == 'PL'){
  //   $task_number = 2;
  // }else if($remarks == 'DC'){
  //   $task_number = 3;
  // }else{
  //   $task_number = 0;
  // }


  if(mysql_num_rows($count_notif) == 0 || mysql_num_rows($count_notif) == 3){
    $function="<h6  onclick='showTrackMap(".$id.")' style='cursor: pointer'><span class='nc-icon nc-pin-3 pull-right'> </span> Show Map</h6>";
  }else{
    $function="";
  }
  return $function;
}

?>
<style type="text/css">
  li .fa{
    color:green;
  }
  #map {
        height: 450px;;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
 /* #track_map {
    height: 450px;;  
    width: 100%;  
   }*/
  .modal-backdrop{
    visibility: hidden !important;
  }
</style>
<div class="content" style="width: 100%;">
  <div class="container-fluid">
      <div class="row">

            
        <?php 
        // require_once('../core/geoplugin.class.php');

        // $geoplugin = new geoPlugin();


        // //locate the IP
        // $geoplugin->locate();
        // $geoplugin->countryName;
        // $geoplugin->city;
       

        if($status == 'D'){
        $fetch = mysql_query("SELECT * FROM tbl_transaction where status='C' and driver_id='$id' ORDER BY `tbl_transaction`.`date_added` ASC");
        while($row = mysql_fetch_array($fetch)){
          $trans_id = $row['trans_id'];

          $total = ($row['price']*$row['qty'])+$row['fee'];
          $count_notif = mysql_query("SELECT * from tbl_track_transaction where trans_id='$trans_id' ");


          if(mysql_num_rows($count_notif)==0){
            $qty_function = '';
            $dl_function = 'onclick="deliverLaundry('.$row['trans_id'].')" style="cursor: help;"';

          }else if($row['qty']==0 && mysql_num_rows($count_notif)==1){
            $qty_function = 'onclick="addQty('.$row['trans_id'].",".$row['merchant_id'].')" style="cursor: help;"';
            $dl_function = '';
          }else{
            $qty_function = '';
            $dl_function = 'onclick="deliverLaundry('.$row['trans_id'].')" style="cursor: help;"';
          }

            if($row['pick_drop_lat'] == '' && $row['pick_drop_long'] == ''){
                $address = getData($row['user_id'],'tbl_user','address','user_id');
            }else{
                $address = getaddress_geocode($row['pick_drop_lat'],$row['pick_drop_long']);
            }

          ?>
            <div class="col-md-4" id="service_id<?php echo $row['trans_id'];?>" >
              <div class="card ">
                  <div class="card-header ">
                      <h4 class="card-title" id="merch_name<?php echo $row['trans_id'];?>"> <?php echo ucwords(getData($row['merchant_id'],'tbl_merchant','name','merchant_id'));?>
                        

                      </h4>
                      <p class="card-category" id="merch_address<?php echo $row['trans_id'];?>"> <?php echo getData($row['merchant_id'],'tbl_merchant','address','merchant_id');?> </p>
                  </div>
                  <div class="card-body ">   
                      <b>Customer Name :</b> <?php echo ucwords(getFullname($row['user_id']));?><br>
                      <b>Customer Address :</b> <?php echo $address;?><br>
                       <b>Customer Number :</b> <?php echo getData($row['user_id'],'tbl_user','contact_number','user_id');?><br>
                      <b>Service Type :</b> <?php echo  getServiceName($row['service_id']);?><br>
                      <b>Category :</b> <?php echo getData($row['service_id'],'tbl_services','category','service_id');?><br>
                      <b>Price:</b> Php <?php echo getData($row['service_id'],'tbl_services','price','service_id')."/".getData($row['service_id'],'tbl_services','packaging','service_id');?><br>
                      <b>Quantity:</b>  <?php echo $row['qty'];?><br>
                      <b>Schedule Pick Up:</b>  <?php echo date('F d,Y g:i a',strtotime($row['sched_date'].' '.$row['sched_time']));;?><br>
                      <b>Schedule Drop-off:</b>  <?php echo date('F d,Y g:i a',strtotime($row['sched_drop_off']));?><br>
                      <b>Delivery Fee:</b>  <?php echo $row['fee'];?><br>
                      <hr>
                      <div class="stats" style="color: #1f77d0;" id="price<?php echo $row['service_id'];?>">
                         Total : Php <?php echo number_format($total,2);?>
                      </div>
                      <hr>
                      <h5>Task</h5>
                      <?=hideShow_map($trans_id);?>
                      <!-- <h6  onclick="showTrackMap(<?=$trans_id?>)" style='cursor: pointer'><span class='nc-icon nc-pin-3 pull-right'> </span>Show Map</h6> -->
                      <!-- <button onclick="showTrackMap(<?=$trans_id?>)">Show Location</button> -->
                      <ol> 
                        <li id="task0" style="margin: 5px;" <?php echo getNotif($trans_id,'PC');?> >  <?= getButton($trans_id,'PC')?> Pick up from Customer</button></li>
                        <li id="task1" style="margin: 5px;" <?php echo $qty_function;?> ><?= getButton($trans_id,'DL')?> Deliver to Laundry Merchant </button></li>
                        <li id="task2" style="margin: 5px;" <?php echo getNotif($trans_id,'PL');?> > <?= getButton($trans_id,'PL')?> Pick up from Laundry Merchant </button></li>
                        <li id="task3" style="margin: 5px;" <?php echo getNotif($trans_id,'DC');?> > <?= getButton($trans_id,'DC')?> Deliver to Customer </button></li>
                      </ol>
                  </div>
              </div>
          </div>
         
        <?php }}?>

        <?php 
        if($status == 'C'){
        $fetch = mysql_query("SELECT * FROM tbl_services ORDER BY `tbl_services`.`price` ASC");
        while($row = mysql_fetch_array($fetch)){
          if($row['service_type'] == 'B'){
            $service_type = 'Basic Items';
            $image = "washing";
          }else if($row['service_type'] == 'D'){
            $service_type = 'Dry Cleaning';
            $image = "dry_cleaning";
          }else{
            $service_type = 'Special Items';
            $image = "blanket";
          }
          $filename = $row['filename'];
          $avg_rate = mysql_fetch_array(mysql_query("SELECT avg(rating) from tbl_rating where service_id = '$row[service_id]'"));

          ?>
            <div class="col-md-4" id="service_id<?php echo $row['service_id'];?>" onclick="addService(<?php echo $row['service_id'].",".$row['merchant_id'];?>)">
              <div class="card ">
                  <div class="card-header ">
                      <h4 class="card-title" id="merch_name<?php echo $row['service_id'];?>"><!-- <?php echo substr(ucwords(getData($row['merchant_id'],'tbl_merchant','name','merchant_id')),0,18);?> -->
                        <?php echo $service_type;?>
                        
                      </h4>
                      <p class="card-category" id="cat<?php echo $row['service_id'];?>"><?php echo $row['category'];?></p>
                  </div>
                  <div class="card-body ">   
                      <img src="../assets/img/services/<?=$filename?>" style="width: 100%;">
                      <hr>
                      <div class="stats" style="color: #1f77d0;" id="price<?php echo $row['service_id'];?>">
                         <label for="radio5" style="font-size: 20px;color: orange;"> <?php echo number_format($avg_rate[0],1);?>&#9733;</label> <br>
                          <i class="fa fa-tag"></i> Php <?php echo $row['price']."/".$row['packaging'];?>
                      </div>
                  </div>
              </div>
          </div>
            <?php }}?>
        
         
      
        


      </div>
  </div>
</div>
<div class='modal fade' id='map_modal' tabindex="-1" data-backdrop='static'>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        
      </div>
      <div class='modal-body'>
          <div id="map"></div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Close</button>
         <button type="button" class="btn btn-success btn-simple" data-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>
<!-- <div class='modal fade' id='track_map_modal' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        
      </div>
      <div class='modal-body'>
          <div id="track_map"></div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success btn-simple btn-sm pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div> -->
<script type="text/javascript">
  $(document).ready(function(){
     $('#map_modal').on('hidden.bs.modal', function (e) {
                $("#addServiceCart").modal();
              });
  });
 
    
    function showTrackMap(transid){
      window.location = 'index.php?page=track-location&transid='+transid;
      //locate(transid);
    }
    
   
  
  function addService(id,merchant_id){
    var merch_name = $("#merch_name"+id).text();
    var category = $("#cat"+id).text();
    var price = $("#price"+id).text();
    $("#service_id").val(id);
    $("#notif").html("<b>"+merch_name+"</b> <br>"+category+" <br> "+price)
    $("#addServiceCart").modal("show");
    $('#btn_confirm').attr('onClick', 'confirm('+id+','+merchant_id+')');
    $("#rating").html("<span class='fa fa-spin fa-spinner'></span> Fetching ratings...Please Wait");
    getServiceRating(id);
  }

  function getServiceRating(id){
     $.ajax({
      url:"../ajax/getRatingStar.php",
      type:"POST",
      data:{
          id:id
        }
      ,success:function(data){
        var o = JSON.parse(data);
        var start = 0;

        $("#rating").html("");
        $("#comments").html(o.comment);

        $("#star_rating").html("<span style='float: right;'>"+o.avg+"<label for='radio5' style='font-size: 12px;color: orange;'>&#9733;</label></span>");
        while(start<=5){
            if(start == 0){
            var rating = o.rating_0;
           }else if(start == 1){
            var rating = o.rating_1;
           }else  if(start == 2){
             var rating = o.rating_2;
           }else  if(start == 3){
            var rating = o.rating_3;
           }else  if(start == 4){
            var rating = o.rating_4;
           }else  if(start == 5){
            var rating = o.rating_5;
           }

           if(start == 0){
             $("#rating").append('<button class="class="btn btn-default btn-sm" "> All <label for="radio5" style="font-size: 20px;color: orange;">&#9733;</label> ('+rating+')</button>');
           }else{
               $("#rating").append('<button class="class="btn btn-default btn-sm" >'+start+' <label for="radio5" style="font-size: 20px;color: orange;">&#9733;</label> ('+rating+')</button>');
           }
          
          start++;
        }
       
      }
  });
  }

  function confirm(id,merchant_id){
    var note = $("#note").val();
    var sched_date = $("#sched_date").val();
    var sched_time = $("#sched_time").val();
    var drop_date = $("#drop_date").val();
    var drop_time = $("#drop_time").val();
    var lat = $("#lat").val();
    var lang = $("#long").val();
    //var addr = $("#addrName").val();


    if(sched_time == '' || sched_date == '' || drop_date == '' || drop_time == ''){
       alert_custom("fa fa-exclamation","Aw Snap!","Schedule pick up is empty","warning");
    }else if(lat == '' || lang == ''){
      alert_custom("fa fa-exclamation","Aw Snap!","Please Wait, Location is still processing","warning");
    }else{
         $.ajax({
        url:"../ajax/addTransaction.php",
        method:"POST",
        data:{
          id:id,
          merchant_id:merchant_id,
          sched_date:sched_date,
          sched_time:sched_time,
          drop_date:drop_date,
          drop_time:drop_time,
          note:note,
          lat:lat,
          lang:lang
        },
        success: function(data){
        if(data == 1){
          success_add();
          $("#addServiceCart").modal("hide");
        }else if(data == 3){
            $("#addServiceCart").modal("hide");
            alert_custom("fa fa-exclamation","Aw Snap!","Schedule Pick up is not allowed","warning");
        }else{
          failed_query();
        }
        }
      });
    }
 
  }

  function addQty(id){
    $("#modalQty").modal("show");
    $('#btn_save').attr('onClick', 'save('+id+')');
  }

  function save(id){
    var qty = $("#qty").val();
    $.ajax({
        url:"../ajax/addQty.php",
        method:"POST",
        data:{
          id:id,
          qty:qty
        },
        success: function(data){
        if(data == 1){
          success_add();
          $("#addServiceCart").modal("hide");
          location.reload();
        }else{
          failed_query();
        }
        }
      });
  }

   function deliverLaundry(id,remarks){
    var pastTask = remarks - 1;

    if(remarks == 0){
       $("#modalNotif").modal("show");
    }else{
       if($('#task'+pastTask+' span').hasClass('fa')){
        $("#modalNotif").modal("show");
      }
    }
   




    if(remarks == 0){
      var notif = $("#notif_task").html("Have you already pick up from Customer?");
      $('#btn_save_notif').attr('onClick', 'saveNotif('+id+',"PC")');
    }else if(remarks == 1){
      var notif = $("#notif_task").html("Have you already deliver to laundry merchant?");
      $('#btn_save_notif').attr('onClick', 'saveNotif('+id+',"DL")');
    }else if(remarks == 2){
      var notif = $("#notif_task").html("Have you already pick up from laundry merchant?");
     $('#btn_save_notif').attr('onClick', 'saveNotif('+id+',"PL")');
    }else if(remarks == 3){
      var notif = $("#notif_task").html("Have you already deliver to customer?");
      $('#btn_save_notif').attr('onClick', 'saveNotif('+id+',"DC")');
    }else{

    }
  }

  function saveNotif(id,remarks){
    $.ajax({
        url:"../ajax/addNotif.php",
        method:"POST",
        data:{
          id:id,
          remarks:remarks
        },
        success: function(data){
        if(data == 1){
          success_add();
          $("#addServiceCart").modal("hide");
          location.reload();
        }else{
          failed_query();
        }
        }
      });
  }
 function locateME(){
                var optionChecked = $("input[name='locateMe']:checked").val();
                if(optionChecked == 'rl'){
                  getRegisteredLocation();
                }else{
                  showMap();
                }
              }
  function getRegisteredLocation(){
    $.ajax({
      url:"../ajax/getLatLangRL.php",
      type:"POST",
      success:function(data){
        var splt_data = data.split("-");
        $("#lat").val(splt_data[0]);
        $("#long").val(splt_data[1]);
      }
    });
  }
  
  function showMap(){
    $("#addServiceCart").modal('hide');
    $("#map_modal").modal();
    initialize();
  }
  // function geocodeLatLng(geocoder, infowindow, latlng_v2){
  //   const latlngStr = latlng_v2.split(",", 2);
    
  //   const latlng = {
  //     lat: parseFloat(latlngStr[0]),
  //     lng: parseFloat(latlngStr[1]),
  //   };
  //   geocoder.geocode({ location: latlng }, (results, status) => {
  //     if (status === "OK") {
  //       if (results[1]) {
          
  //         //infowindow.setContent(results[0].formatted_address);

     
  //         $("#addrName").val(results[0].formatted_address);
  //       } else {
  //         window.alert("No results found");
  //       }
  //     } else {
  //       window.alert("Geocoder failed due to: " + status);
  //     }
  //   });

  // }
  //  function getLocation() {
  //     if (navigator.geolocation) {
  //         navigator.geolocation.getCurrentPosition();
  //     } else { 
  //         $("#map").html("Geolocation is not supported by this browser.");
  //     }
  // }
    var position = [10.6840, 122.9563];
  function initialize() { 
    
      var latlng = new google.maps.LatLng(position[0], position[1]);
      var myOptions = {
          zoom: 20,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };


      map = new google.maps.Map(document.getElementById("map"), myOptions);
     // var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
      marker = new google.maps.Marker({
          position: latlng,
          map: map,
          icon: "../assets/img/ic_launcher-removebg-preview.png",
          title: "Latitude:"+position[0]+" | Longitude:"+position[1]
      });

      

      google.maps.event.addListener(map, 'click', function(event) {
          var result = [event.latLng.lat(), event.latLng.lng()];
          transition(result);
      });
  }



  var numDeltas = 100;
  var delay = 10; //milliseconds
  var i = 0;
  var deltaLat;
  var deltaLng;

  function transition(result){
      i = 0;
      deltaLat = (result[0] - position[0])/numDeltas;
      deltaLng = (result[1] - position[1])/numDeltas;
      moveMarker();
      $("#lat").val(position[0]);
      $("#long").val(position[1]);


      
  }

  function moveMarker(){
      position[0] += deltaLat;
      position[1] += deltaLng;
      var latlng = new google.maps.LatLng(position[0], position[1]);
      marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
      marker.setPosition(latlng);
      if(i!=numDeltas){
          i++;
          setTimeout(moveMarker, delay);
      //     $.ajax({
      //   url: "https://maps.googleapis.com/maps/api/geocode/json?latlng="+position[0]+","+position[1]+"&key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0",
      //   success:function(result){
      //     console.log(result.results[0]);
      //   }
      // })
      }
  }

</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0"> </script>


