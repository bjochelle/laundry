<?php 
include "../core/config.php";
  $status = $_SESSION['status'];
?>
<style type="text/css">
  h2{
    color:white;
    text-align: center;
  }
  #map {
        height: 600px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>
<div class="content" style="width: 100%;">
  <div class="container-fluid">
      <div class="row">
          <div class='col-md-12'>
          <?php 
            if(checkdnsrr('php.net')){
              echo '<div id="map" style="margin-top: 50px;"></div>';
            }else{
              echo "<h4 style='text-align: center;color: red' class='animated shake'> <span class='fa fa-exclamation'></span> Unable to load Google Map, Please Connect to Internet.<h4>";
            }
          ?>
          </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  var marker;
  function initMap() {
    var locations = [<?php 
                        $getAlllocations = mysql_query("SELECT * FROM `tbl_merchant`");
                        while($fetchrow = mysql_fetch_array($getAlllocations)){
                            $response = array();
                            $response[] = "<strong>Location: </strong>".$fetchrow["address"]."<br><br>"."<strong>Business Name: </strong> ".$fetchrow["name"];
                            $response[] = $fetchrow["location_lat"];
                            $response[] = $fetchrow["location_long"];
                            $response[] = $fetchrow["name"];
                            echo json_encode($response).",";
                        }
                        ?>];
    //alert(locations);
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: new google.maps.LatLng(10.6840,122.9563),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        animation: google.maps.Animation.DROP
        
      });

     // marker.addListener('click', toggleBounce);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));

    }
    
    //marker.addListener('click', toggleBounce);
    }
    
    // function toggleBounce() {
    //     if (marker.getAnimation() !== null) {
    //       marker.setAnimation(null);
    //     } else {
    //       marker.setAnimation(google.maps.Animation.BOUNCE);map-icon-laundry
    //     }
    //   }
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=initMap"> </script>
