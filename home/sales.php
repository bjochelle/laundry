<style type="text/css">
  .form-control{
    border: 1px solid #495057;
  }
  .modal-dialog{
     margin: 0 auto;
  }
  .modal-content{
        top: -100px;
  }
</style>
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">Sales Report</h4>
        </div>
        <div class="card-body">
          <div class="row">
          <?php if($status == 'A'){?>
            <div class="col-md-6" style="margin-bottom: 20px;">
                <span class="input-group-addon" style="width: 30%;float: left;"><strong> Filter By : <span style="color:red;">*</span></strong></span>  
                <select class="form-control" id="filter_by" name="filter_by" style="width: 70%;float: left;" onchange="getName()">
                  <option value="">Select</option>
                  <option value="L">Merchant</option>
                  <option value="D">Driver</option>
                </select>
            </div>
            <div class="col-md-6" style="margin-bottom: 20px;">
                <span class="input-group-addon" style="width: 30%;float: left;"><strong> Name : <span style="color:red;">*</span></strong></span>  
                <select class="form-control" id="name" name="name" disabled="" style="width: 70%;float: left;">
                  <option value="">Select</option>
                </select>
            </div>
          <?php }?>
            <div class="col-md-4">
              <span class="input-group-addon" style="width: 30%;float: left;"><strong> From : <span style="color:red;">*</span></strong></span> 
                <input type="date" class="form-control sd" id="sd" autocomplete="off" style="width: 70%;float: left;">
            </div>
            <div class="col-md-4">
                <span class="input-group-addon" style="width: 30%;float: left;"><strong> To : <span style="color:red;">*</span></strong></span>  
                <input type="date" class="form-control end_date" id="ed" autocomplete="off" style="width: 70%;float: left;">
            </div>
            
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary" onclick="gen_report()">Generate</button>
            </div>
          </div>
          <br><br>
          <div class="row" >
            <div class="card-body" id="report_data"></div>
          </div>

        </div>
    </div>
</div>
<script type="text/javascript">

  function getName(){
    var filter_by = $("#filter_by").val();

    if(filter_by == ''){
      $("#name").prop("disabled",true)
      $("#name").html('<option value="">Select</option>');
    }else{
      $.ajax({
        url:"../ajax/getSelectionName.php",
        method:"POST",
        data:{
          filter_by:filter_by
        },
        success: function(data){
          $("#name").prop("disabled",false)
          $("#name").html(data);
        }
      });
    }
  }

  function gen_report(){
    var sd = $("#sd").val();
    var ed = $("#ed").val();
    var status = '<?php echo $status;?>';
    if(status == 'D'){
      var id = '<?php echo $id;?>';
      var filter_by = 'D';
    }else{
       var id = $("#name").val();
       var filter_by = $("#filter_by").val();
    }
  

    if(sd == "" || ed == "" || id == '' ){
      alert("Please Fill in the form");
    }else{
       $.ajax({
          url:"../ajax/sales_report.php",
          method:"POST",
          data:{
            sd:sd,
            ed:ed,
            id:id,
            filter_by:filter_by
          },
          success: function(data){
              $("#report_data").html(data);
          }
        });
    }
  }

  function sendEmail(){
    var id = $("#name").val();
    var sd = $("#sd").val();
    var ed = $("#ed").val();

    $.ajax({
      url:"../ajax/send_report.php",
      method:"POST",
      data:{
        id:id,
        sd:sd,
        ed:ed
      },success:function(data){

        if(data == 1){
            alert_custom("fa fa-check-circle","All Good!","Successfully send email","success");
        }else{
            alert_custom("fa fa-exclamation","Aw Snap!","Unable to send email","warning");
        }

      }
    });
  }
</script>