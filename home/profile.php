<?php

    include "../core/config.php";
    $id = $_SESSION['id'];
?>
<style type="text/css">
    .cr-slider-wrap{
        display: none;
    }
</style>
 <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/croppie.min.css">
<!--   <script src="../assets/js/jquery.min.js"></script> -->
  <script src="../assets/js/croppie.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Profile </h4>
                    </div>

                    <div class="card-body">
                        <form id="update_profile">
                            <input type="hidden" id="user_id" name="user_id" value="<?php echo $id;?>">
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                     <div class="form-group">
                                        <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name" name="fname" value="<?php echo getDetails($id,'tbl_user','fname');?>">
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" placeholder="Last Name" name="lname" value="<?php echo getDetails($id,'tbl_user','lname');?>">
                                        
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" placeholder="Username" name="un" value="<?php echo getDetails($id,'tbl_user','un');?>">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Birthdate</label>
                                        <input type="date" class="form-control" name="bday" placeholder="" value="<?php echo getDetails($id,'tbl_user','bday');?>">
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo getDetails($id,'tbl_user','email');?>">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" placeholder="Phone Number" name="contact_number" value="<?php echo getDetails($id,'tbl_user','contact_number');?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control" placeholder="ex(Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09)" name="address" value="<?php echo getDetails($id,'tbl_user','address');?>">
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>
                        <br>
                        <a href="index.php?page=changePassword">Change Password?</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-image">
                        <img src="../assets/img/banner.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="../assets/img/faces/<?php echo getDetails($id,'tbl_user','filename');?> " alt="..." id="upload-demo">
                               
                            </a>
                            <br> <br>
                             <h5 class="title"><?php echo ucwords(getDetails($id,'tbl_user','fname')." ".getDetails($id,'tbl_user','lname'));?></h5>
                            <input type="file" id="image" style="display: none;" required="">
                            <p class="description">
                                <?php echo getDetails($id,'tbl_user','un');?>
                            </p>
                        </div>
                        <input type="hidden" id="user_id"  value="<?php echo $id;?>">
                        <p class="description text-center">
                            <?php echo getDetails($id,'tbl_user','email');?>
                            <br> <?php echo date('F d, Y',strtotime(getDetails($id,'tbl_user','bday')));?>
                            <br> <?php echo getDetails($id,'tbl_user','contact_number');?>
                        </p>
                    </div>
                     <hr>
                    <div class="button-container mr-auto ml-auto">
                        <span  style="position: absolute;top: 210px;"><i class="nc-icon nc-cloud-upload-94" id="upload_image" style="color: black;font-size: 23px;font-weight: bolder;"></i>  Select Image</span>
                          <button class="btn btn-info btn-fill pull-right btn-upload-image" id="btn_upload">Upload Image</button>
                    </div>
                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#upload_image').on('click', function() {
            $('#image').trigger('click');
        });

        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });

        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 150,
                height: 150,
                type: 'square' //square
            },
            boundary: {
                width: 150,
                height: 150
            }
        });

        $('.btn-upload-image').on('click', function (ev) {
            var user_id = $("#user_id").val();
          resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
          }).then(function (img) {
            $.ajax({
              url: "../ajax/upload_image.php",
              type: "POST",
              data: {"image":img,"user_id":user_id},
              success: function (data) {
                // location.reload();
                alert_custom("fa fa-check-circle","All Good!","Image was successfully uploaded","success");

              }
            });
          });
        });

          $("#update_profile").submit(function(e){
        e.preventDefault();
         $.ajax({
            url:"../ajax/update_profile.php",
            method:"POST",
            data:$(this).serialize(),
            success: function(data){
              if(data == 1){
                  success_update();
                location.reload();
             }else{
                failed_query();
            }
            }
          });
        })

    })
</script>