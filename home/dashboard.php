<?php 
include "../core/config.php";
  $status = $_SESSION['status'];
?>
<style type="text/css">
  h2{
    color:white;
    text-align: center;
  }
</style>
<div class="content" style="width: 100%;">
  <div class="container-fluid">
      <div class="row">
        <?php if($status == 'A'){?>
            <div class="col-md-4">
              <div class="card" style="background-color: #fb404b;">
                  <div class="card-header ">
                      <h4 class="card-title">Total no of users</h4>
                  </div>
                  <div class="card-body ">
                    <h2 id="count_users"></h2>
                  </div>
              </div>
          </div>
          <!-- <div class="col-md-4">
              <div class="card" style="background-color: #9368e9;">
                  <div class="card-header" >
                      <h4 class="card-title">Laundry Merchant</h4>
                  </div>
                  <div class="card-body">
                    <h2 id="count_merchant"></h2>
                    
                  </div>
              </div>
          </div> -->
          <div class="col-md-4">
              <div class="card" style="background-color: #007bff;">
                  <div class="card-header">
                      <h4 class="card-title">Driver</h4>
                  </div>
                  <div class="card-body ">
                    <h2 id="count_driver"></h2>

                  </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="card" style="background-color: #ffc107;">
                  <div class="card-header ">
                      <h4 class="card-title">Customer</h4>
                  </div>
                  <div class="card-body ">
                    <h2 id="count_customer"></h2>
                  </div>
              </div>
          </div>
        <?php }?>
          <div class="col-md-4">
              <div class="card" style="background-color: #1dc7ea;">
                  <div class="card-header ">
                      <h4 class="card-title">No. of Booking (today)</h4>
                  </div>
                  <div class="card-body ">
                    <h2 id="count_booking"></h2>
                  </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="card" style="background-color: #28a745;">
                  <div class="card-header ">
                      <h4 class="card-title">Finish Booking (today)</h4>
                  </div>
                  <div class="card-body">
                    <h2 id="count_finish"></h2>
                  </div>
              </div>
          </div>

          <?php if($status == 'A'){?>
          <div class="col-md-12">
              <div class="card" style="background-color: #FF5722;">
                  <div class="card-header ">
                      <h4 class="card-title"> Sales Today</h4>
                  </div>
                  <div class="card-body">
                    <div id="sales_chart"></div>
                  </div>
              </div>
          </div>

          <?php } ?>
          <?php //if($status == 'L'){?>
          <div class="col-md-12">
              <div class="card" style="background-color: #FF5722;">
                  <div class="card-header ">
                      <h4 class="card-title"> Sales by Service Type </h4>
                  </div>
                  <div class="card-body">
                    <div id="sales_pie"></div>
                  </div>
              </div>
          </div>
           <?php //} ?>
      </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    sales_chart();
    sales_pie();
       });

  var eSource,source,source1;  //define my global eventSource Object
      if (!!window.EventSource)  //Check for Browser feature compatibiliity
       {
         var eSource3= new EventSource('../ajax/sse_booking.php?status=A');
        eSource3.onmessage = function(event) {
          document.getElementById("count_booking").innerHTML = event.data;
        };

        var eSource4= new EventSource('../ajax/sse_booking.php?status=F');
        eSource4.onmessage = function(event) {
          document.getElementById("count_finish").innerHTML = event.data;
        };
        
        if('<?php echo $status;?>' == 'A'){
           var eSource= new EventSource('../ajax/sse.php?status=L');
            eSource.onmessage = function(event) {
              document.getElementById("count_merchant").innerHTML = event.data;
            };

            var eSource= new EventSource('../ajax/sse.php?status=A');
            eSource.onmessage = function(event) {
              document.getElementById("count_users").innerHTML = event.data;
            };

            var eSource1= new EventSource('../ajax/sse.php?status=D');
            eSource1.onmessage = function(event) {
              document.getElementById("count_driver").innerHTML = event.data;
            };

            var eSource2= new EventSource('../ajax/sse.php?status=C');
            eSource2.onmessage = function(event) {
              document.getElementById("count_customer").innerHTML = event.data;
            };

        }
        

       

      } else {
        alert("You're browser does not support EventSource needed for this page ");
        // Fallback method perhaps you can use old-school XHR polling
      }


function sales_chart(){

  $("#sales_chart").html("<h4><center><span class='fa fa-spinner fa-spin'></span> Loading graph ...</center></h4>");

  $.getJSON('../ajax/salesChartByMechant.php', function(data){
    //var json_data = JSON.parse(data);
    // Create the chart
    Highcharts.chart("sales_chart", {
      chart: {
        type: 'column'
      },
      title: {
        text: "Sales By Merchant <br> <span style='text-transform: capitalize;'>TODAY </span> "
      },
      xAxis: {
        type: 'category',
        title: {
          // text: 'Laundry Merchant'
        }
      },
      yAxis: {
        title: {
          text: 'Sales'
        }
      },
      legend: {
          enabled: false
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: true,
                  format: '{point.y:.0f}'
              }
          }
      },

      tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">{point.name} </span> <span style="font-size:11px">{series.name}</span>: <b>{point.y:.0f}</b><br/>'
      },

     series: data['series'],
    });
  });
}

function sales_pie(){

  $("#sales_pie").html("<h4><center><span class='fa fa-spinner fa-spin'></span> Loading graph ...</center></h4>");

  $.getJSON('../ajax/salesChartByServiceType.php', function(data){
    //var json_data = JSON.parse(data);
    // Create the chart
    Highcharts.chart("sales_pie", {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: "Sales By Service Type <br> <span style='text-transform: capitalize;'>TODAY </span> "
      },
       accessibility: {
        announceNewData: {
            enabled: true
        },
        point: {
            valueSuffix: '%'
        }
    },
      xAxis: {
        type: 'category',
        title: {
          text: 'Service Type'
        }
      },
      yAxis: {
        title: {
          text: 'Sales'
        }
      },
      legend: {
          enabled: false
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: true,
                  format: '{point.name}: {point.y:.1f}%'
              }
          }
      },

      tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">{point.name} </span> <span style="font-size:11px">{series.name}</span>: <b>{point.y:.0f}</b><br/>'
      },

     series: data['series'],
     drilldown: {
      series: data['drilldown']
    }
    });
  });
}

</script>
