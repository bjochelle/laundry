<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of Laundry Merchant 
              <button type="submit" class="btn btn-info btn-fill pull-right" style="float: right" onclick="addUser()">Add</button>
        </h4>
        </div>
        <div class="card-body table-full-width table-responsive">

            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                    <th>Name</th>
                    <th>Contact Number</th>
                    <th>Date Added</th>
                    <th>Action</th>

                </tr></thead>
                <tbody>

                </tbody>
            </table>

        </div>
    </div>
</div>
<script type="text/javascript">
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      var status = 'L';
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"../ajax/datatables/dt_user.php",
          "dataSrc":"data",
          "data":{
              status:status
            }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"name"
          },
          {
            "data":"contact_number"
          },
          {
            "data":"date_added"
          },          
          {
            "mRender": function(data,type,row){
              return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='" + row.id+ "' onclick='viewDetails("+row.id+")'><span class='fa fa-eye'></span></button><button class='btn btn-warning btn-sm' data-toggle='tooltip' title='Edit Details' value='" + row.id+ "' onclick='editDetails("+row.id+")'><span class='fa fa-edit'></span></button></center>";
              //<button class='btn btn-success btn-sm' data-toggle='tooltip' title='Archive Merchant' value='" + row.id+ "' onclick='archive("+row.id+")'><span class='fa fa-archive'></span></button>
            }
          }
        ]
      });
    }

  function addUser(){
        window.location.replace("index.php?page=addUser&&id=L");
  }
  function viewDetails(id){
        window.location.replace("index.php?page=viewDetails&&id="+id);
  }
  function editDetails(id){
        window.location.replace("index.php?page=editDetails&&id="+id);
  }

  function archive(id){
     $.ajax({
        url:"../ajax/archive_user.php",
        method:"POST",
        data:{id:id},
        success: function(data){
          if(data == 1){
            alert_custom("fa fa-check-circle","All Good!","Successfully Archived.","success");
         }else{
            failed_query();
        }
        }
      });
  }
$(document).ready(function (){
  getData();
});
</script>