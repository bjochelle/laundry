<?php 
$id = $_SESSION['id'];
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Roboto:500,100,300,700,400);
*{
  margin: 0;
  padding: 0;
  font-family: roboto;
}

.cont{
  width: 93%;
  max-width: 350px;
  text-align: center;
  margin: 4% auto;
  padding: 30px 0;
  background: #111;
  color: #EEE;
  border-radius: 5px;
  border: thin solid #444;
  overflow: hidden;
}

hr{
  margin: 20px;
  border: none;
  border-bottom: thin solid rgba(255,255,255,.1);
}

div.title{
  font-size: 2em;
}

h1 span{
  font-weight: 300;
  color: #Fd4;
}

div.stars{
  width: 270px;
  display: inline-block;
}

input.star{
  display: none;
}

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content:'\f005';
  color: #FD4;
  transition: all .25s;
}


input.star-5:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before {
  color: #F62;
}

label.star:hover{
  transform: rotate(-15deg) scale(1.3);
}

label.star:before{
  content:'\f006';
  font-family: FontAwesome;
}

.rev-box{
  overflow: hidden;
  height: 0;
  width: 100%;
  transition: all .25s;
}

textarea.review1{
  background: #222;
  border: none;
  width: 100%;
  max-width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  color: #EEE;
}

label.review1{
  display: block;
  transition:opacity .25s;
}

textarea.review{
  background: #222;
  border: none;
  width: 100%;
  max-width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  color: #EEE;
}

label.review{
  display: block;
  transition:opacity .25s;
}



input.star:checked ~ .rev-box{
  height: 125px;
  overflow: visible;
}






</style>
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of Booking 
        </h4>
        </div>
        <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                   <!--  <th>Merchant</th>
                    <th>Merchant Address</th> -->
                    <th>Service Type</th>
                    <th>Status</th>
                    <th>Remarks</th>
                    <th>Total Price</th>
                    <th>Pick up</th>
                    <th>Action</th>

                </tr></thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
</div>
<script type="text/javascript">
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      var id = <?php echo $id ;?>;
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"../ajax/datatables/dt_my_trans.php",
          "dataSrc":"data",
          "data":{
            id:id
          }
        },
        "columns":[
          {
            "data":"count"
          },
          // {
          //   "data":"merchant"
          // },
          // {
          //   "data":"merch_addr"
          // },
          {
            "data":"service"
          },
          {
            "data":"status"
          },
          {
            "data":"remarks"
          },
          {
            "data":"price"
          }, 
          {
            "data":"date"
          },         
          {
            "mRender": function(data,type,row){
              if(row.stat == 'S'){
                return "<center><button class='btn btn-danger btn-sm' data-toggle='tooltip' title='Cancel' value='"+row.id+ "' onclick='cancelBooking("+row.id+")'><span class='fa fa-times'></span>Cancel</button></center>";
              }else if(row.stat == 'F' && row.allow_rating == 0){
                return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.id+ "' onclick='viewDetails("+row.id+")'><span class='fa fa-eye'></span> View</button><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Rate' value='"+row.id+ "' onclick='rate("+row.id+","+row.service_id+")'><span class='fa fa-check-circle'></span> Rate</button></center>";
              }else{
                if(row.count_notif == 0 || row.count_notif == 3){
                return "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.id+ "' onclick='viewDetails("+row.id+")'><span class='fa fa-eye'></span> View</button><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.id+ "' onclick='showMapLocation("+row.id+")'><span class='fa fa-eye'></span> Show Map</button>";
              }else{
                 return "<button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.id+ "' onclick='viewDetails("+row.id+")'><span class='fa fa-eye'></span> View</button>";
              }
              }
              
            }
          }
        ]
      });
    }

 function showMapLocation(id){
  window.location = 'index.php?page=track-location-c&id='+id;
 }
$(document).ready(function (){
  getData();
});

 function confirm(id){
      $("#modalConfirmBooking").modal("show");
      $('#btn_confirm').attr('onClick', 'confirmBooking('+id+')');
}

 function rate(id,service_id){
      $("#modalRate").modal("show");
      $('#btn_submit').attr('onClick', 'sendRating('+id+','+service_id+')');
}

function sendRating(id,service_id){
  var rate  =  $("#rateValue").val();
  var comment  = $("#comment").val();
  var driver_rate  = $("#rateValue1").val();
  var driver_comment  = $("#driver_comment").val();


   $.ajax({
      url:"../ajax/addRate.php",
      method:"POST",
      data:{
        id:id,
        service_id:service_id,
        rate:rate,
        driver_rate:driver_rate,
        comment:comment,
        driver_comment:driver_comment

      },
      success: function(data){
      if(data == 1){
        alert_custom("fa fa-check-circle","All Good!","Rating was successfully added","success");
        $("#modalRate").modal("hide");
        getData();
          $("#rateValue").val('');
          $("#comment").val('');
          $("#rateValue1").val('');
          $("#driver_comment").val('');
      }else{
        failed_query();
      }
    }
  });
}

 function rate(id,service_id){
      $("#modalRate").modal("show");
      $('#btn_submit').attr('onClick', 'sendRating('+id+','+service_id+')');
}

 function rateValue(id){
  $("#rateValue").val(id);
   // $('input[type="checkbox"]').attr("checked", "checked");

}

 function rateValue1(id){
  $("#rateValue1").val(id)
}




function viewDetails(id){
  $("#modalBookingDetails").modal("show");
   $.ajax({
      url:"../ajax/getTransDetails.php",
      type:"POST",
      data:{
          id:id
        }
      ,success:function(data){
        var o = JSON.parse(data);
        $("#merch_name").html(o.merch_name);
        $("#merch_address").html(o.merch_address);
        $("#drivers_name").html(o.drivers_name);
        $("#service_type").html(o.service_type);
        $("#view_category").html(o.category);
        $("#view_price").html(o.price);
        $("#view_qty").html(o.qty);
        $("#total").html(o.total);
        $("#task0").html(o.PC);
        $("#task1").html(o.DL);
        $("#task2").html(o.PL);
        $("#task3").html(o.DC);
        $("#sched_pick").html(o.sched_pick);
        $("#sched_drop").html(o.sched_drop);
        $("#fee").html(o.fee);


      }
  });
}

 function cancelBooking(id){
   $.ajax({
      url:"../ajax/cancelBooking.php",
      method:"POST",
      data:{
        id:id
      },
      success: function(data){
      if(data == 1){
        alert_custom("fa fa-check-circle","All Good!","Booking was successfully cancelled","success");
        $("#modalConfirmBooking").modal("hide");
        getData();
      }else{
        failed_query();
      }
    }
  })
}


 function confirmBooking(id){

   $.ajax({
      url:"../ajax/confirmDriverBooking.php",
      method:"POST",
      data:{
        id:id
      },
      success: function(data){
      if(data == 1){
        alert_custom("fa fa-check-circle","All Good!","Booking was successfully confirmed","success");
        $("#modalConfirmBooking").modal("hide");
        getData();

      }else{
        failed_query();
      }
    }
  })
}
</script>