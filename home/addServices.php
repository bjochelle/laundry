<?php 
include "../core/config.php";

$service_id = $_GET['id'];



?>
<style type="text/css">
  li .fa{
    color:green;
  }
  
</style>
<div class="content" style="width: 100%;">
  <div class="container-fluid">
      <div class="row">
       

        <?php 
        if($status == 'C'){
        $fetch = mysql_query("SELECT * FROM tbl_services where service_id='$service_id' ORDER BY `tbl_services`.`price` ASC");
        while($row = mysql_fetch_array($fetch)){
          if($row['service_type'] == 'B'){
            $service_type = 'Basic Items';
            $image = "washing";
          }else if($row['service_type'] == 'D'){
            $service_type = 'Dry Cleaning';
            $image = "dry_cleaning";
          }else{
            $service_type = 'Special Items';
            $image = "blanket";
          }

          $avg_rate = mysql_fetch_array(mysql_query("SELECT avg(rating) from tbl_rating where service_id = '$row[service_id]'"));

          ?>
            <div class="col-md-3" id="service_id<?php echo $row['service_id'];?>" onclick="addService(<?php echo $row['service_id'].",".$row['merchant_id'];?>)">
              <div class="card ">
                  <div class="card-header ">
                      <h4 class="card-title" id="merch_name<?php echo $row['service_id'];?>"><?php echo substr(ucwords(getData($row['merchant_id'],'tbl_merchant','name','merchant_id')),0,18);?></h4>
                      <p class="card-category" id="cat<?php echo $row['service_id'];?>"><?php echo $service_type." (".$row['category'];?>)</p>
                  </div>
                  <div class="card-body ">   
                      <img src="../assets/img/elements/<?php echo $image;?>.png" style="width: 100%;">
                      <hr>
                      <div class="stats" style="color: #1f77d0;" id="price<?php echo $row['service_id'];?>">
                         <label for="radio5" style="font-size: 20px;color: orange;"> <?php echo number_format($avg_rate[0],1);?>&#9733;</label> <br>
                          <i class="fa fa-tag"></i> Php <?php echo $row['price']."/".$row['packaging'];?>
                      </div>
                    <input type="hidden" id="merchant_id" value="<?php echo $row['merchant_id'];?>">

                  </div>
              </div>
          </div>
        <?php }}?>
        <div class="col-md-9">
              <div class="card ">
                  <div class="card-body ">   
                      <input type="hidden" id="service_id" value="">
                      <h5 id="notif"> </h5>
                        <p style="color:#007bff;">Are you want to book this service?</p>

                         <label>Schedule Pick-up : </label>
                        <input type='date' id="sched_date">
                        <input type='time' id="sched_time">

                        <br><label>Schedule Drop-off : </label>
                        <input type='date' id="drop_date">
                        <input type='time' id="drop_time">
                        <br> <label>Note : </label>
                        <textarea class="form-control" id="note"></textarea>
                   
                         <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-simple" id="btn_confirm" onClick='confirm()'>Yes</button>
                            <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">No</button>
                        </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12" style="float: left;">
              <div class="container">
                    <h5>Service Ratings <span id="star_rating"> </span></h5>
                  <div id="rating"></div><br>
                  <div id="comments" class="row"></div>
              </div>
          </div>


      </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
      getServiceRating();
  });

  function getServiceRating(){

    var id = '<?php echo $service_id;?>';
     $.ajax({
      url:"../ajax/getRatingStar.php",
      type:"POST",
      data:{
          id:id
        }
      ,success:function(data){
        var o = JSON.parse(data);
        var start = 0;

        $("#rating").html("");
        $("#comments").html(o.comment);

        $("#star_rating").html("<span style='float: right;'>"+o.avg+"<label for='radio5' style='font-size: 20px;color: orange;'>&#9733;</label></span>");
        while(start<=5){
            if(start == 0){
            var rating = o.rating_0;
           }else if(start == 1){
            var rating = o.rating_1;
           }else  if(start == 2){
             var rating = o.rating_2;
           }else  if(start == 3){
            var rating = o.rating_3;
           }else  if(start == 4){
            var rating = o.rating_4;
           }else  if(start == 5){
            var rating = o.rating_5;
           }

           if(start == 0){
             $("#rating").append('<button class="class="btn btn-default btn-sm" style="margin-right: 5px;"> All <label for="radio5" style="font-size: 20px;color: orange;">&#9733;</label> ('+rating+')</button>');
           }else{
               $("#rating").append('<button class="class="btn btn-default btn-sm" style="margin-right: 5px;">'+start+' <label for="radio5" style="font-size: 20px;color: orange;">&#9733;</label> ('+rating+')</button>');
           }
          
          start++;
        }
       
      }
  });
  }


  function confirm(){
    var id = '<?php echo $service_id;?>';
    var merchant_id = $("#merchant_id").val();
    var note = $("#note").val();
    var sched_date = $("#sched_date").val();
    var sched_time = $("#sched_time").val();
    var drop_date = $("#drop_date").val();
    var drop_time = $("#drop_time").val();

    if(sched_time == '' || sched_date == '' || drop_date == '' || drop_time == ''){
       alert_custom("fa fa-exclamation","Aw Snap!","Schedule pick up is empty","warning");
    }else{
         $.ajax({
        url:"../ajax/addTransaction.php",
        method:"POST",
        data:{
          id:id,
          merchant_id:merchant_id,
          sched_date:sched_date,
          sched_time:sched_time,
          drop_date:drop_date,
          drop_time:drop_time,
          note:note
        },
        success: function(data){
        if(data == 1){
          success_add();
          $("#myModal1").modal("hide");
        }else if(data == 3){
            alert_custom("fa fa-exclamation","Aw Snap!","Schedule Pick up is not allowed","warning");
        }else{
          failed_query();
        }
        }
      });
    }
 
  }



  function addQty(id){
    $("#modalQty").modal("show");
    $('#btn_save').attr('onClick', 'save('+id+')');
  }

  function save(id){
    var qty = $("#qty").val();
    $.ajax({
        url:"../ajax/addQty.php",
        method:"POST",
        data:{
          id:id,
          qty:qty
        },
        success: function(data){
        if(data == 1){
          success_add();
          $("#modalQty").modal("hide");
          location.reload();
        }else{
          failed_query();
        }
        }
      });
  }


</script>
