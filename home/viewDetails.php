<?php

    include '../core/config.php';
    
    $id = $_GET['id'];
    $getStatus = getDetails($id,'tbl_user','status');
    if($getStatus == 'D'){
        $user_info = "Driver";
    }else{
        $user_info = "Laundry Merchant";
    }
?>
<style type="text/css">
    a:hover{
        text-decoration: none;
    }
</style>

<div class="content" style="width: 100%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-body">
                            <?php if($getStatus == 'D'){?>
                             
                                <h4 class="card-title">Vehicle Information </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                     <div class="form-group">
                                        <label>OR CR</label>
                                    <span class="form-control"> <?php echo getDetails($id,'tbl_driver','or_cr');?> </span>
                                    </div>
                                </div>
                                <div class="col-md-8 px-1">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_driver','motor_desc');?> </span>
                                    </div>
                                </div>
                            </div>
                             <hr>

                                <h4 class="card-title">Requirements </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">Driver's License</label>
                                         <span class="form-control"> <?php echo getDetails($id,'tbl_driver','drivers_license');?> </span>
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Police Clearance</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_driver','police_clearance');?> </span>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>NBI Clearance</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_driver','nbi_clearance');?> </span>
                                    </div>
                                </div>
                            </div>
                        <?php }else{?>
                                  <h4 class="card-title">Merchant Information </h4>
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="form-group">
                                        <label>Business Name</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','name');?> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Merchant Address</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','address');?> </span>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                     <div class="form-group">
                                        <label>Email Address</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','email');?> </span>
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','contact_number');?> </span>
                                    </div>
                                </div>
                            </div>
                             <hr>

                                <h4 class="card-title">Requirements </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">DTI</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','dti');?> </span>
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>BIR 2303</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','bir');?> </span>
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>TIN NO.</label>
                                        <span class="form-control"> <?php echo getDetails($id,'tbl_merchant','tin');?> </span>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                            <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-image">
                        <img src="../assets/img/banner.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="../assets/img/faces/<?php echo getDetails($id,'tbl_user','filename');?> " alt="...">
                                <h5 class="title"><?php echo ucwords(getDetails($id,'tbl_user','fname')." ".getDetails($id,'tbl_user','lname'));?></h5>
                            </a>
                            
                            <p class="description">
                                <?php echo getDetails($id,'tbl_user','un');?>
                            </p>
                        </div>
                        <p class="description text-center">
                            <?php echo getDetails($id,'tbl_user','email');?>
                            <br> <?php echo date('F d, Y',strtotime(getDetails($id,'tbl_user','bday')));?>
                            <br> <?php echo getDetails($id,'tbl_user','contact_number');?>
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>