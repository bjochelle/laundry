<?php
    $get_user = $_GET['id'];

    if($get_user == 'D'){
        $user_info = "Driver";
    }else{
        $user_info = "Laundry Merchant";
    }
?>
<style type="text/css">
    .cr-slider-wrap{
        display: none;
    }
</style>
 <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/croppie.min.css">
<!--   <script src="../assets/js/jquery.min.js"></script> -->
  <script src="../assets/js/croppie.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add <?php echo $user_info;?> Profile </h4>
                        <input type="hidden" id="get_user" value="<?php echo $get_user;?>">
                    </div>

                    <div class="card-body">
                        <form id="AddDriverForm">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                     <div class="form-group">
                                        <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name" name="fname">
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" placeholder="Last Name" name="lname">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Birthdate</label>
                                        <input type="date" class="form-control" name="bday" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" placeholder="Email" name="email">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" placeholder="Phone Number" name="contact_number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control" placeholder="ex(Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09)" name="address">
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <?php if($get_user == 'D'){?>
                             
                                <h4 class="card-title">Vehicle Information </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                     <div class="form-group">
                                        <label>OR CR</label>
                                    <input type="text" class="form-control" placeholder="OR CR" name="or_cr">
                                    </div>
                                </div>
                                <div class="col-md-8 px-1">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea type="text" class="form-control" name="motor_desc"></textarea>
                                        
                                    </div>
                                </div>
                            </div>
                             <hr>

                                <h4 class="card-title">Requirements </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">Driver's License</label>
                                        <input type="text" class="form-control" placeholder="Driver's License" name="drivers_license">
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Police Clearance</label>
                                        <input type="text" class="form-control" placeholder="Police Clearance" name="police_clearance">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>NBI Clearance</label>
                                        <input type="text" class="form-control" placeholder="NBI Clearance" name="nbi_clearance">
                                    </div>
                                </div>
                                 <!-- <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">Delivery Fee</label>
                                        <input type="number" class="form-control" name="fee" min="0" value="60" disabled="">
                                    </div>
                                </div> -->
                                 <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" placeholder="Username" name="un">
                                        
                                    </div>
                                </div>
                             
                            </div>
                        <?php }else{?>
                                  <h4 class="card-title">Merchant Information </h4>
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="form-group">
                                        <label>Business Name</label>
                                    <input type="text" class="form-control" placeholder="Business Name" name="merchant_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Merchant Address</label>
                                        <textarea type="text" class="form-control" name="merchant_addr"></textarea>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                     <div class="form-group">
                                        <label>Email Address</label>
                                    <input type="email" class="form-control" placeholder="Email Address" name="buss_email">
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input type="text" class="form-control" placeholder="Contact Number" name="buss_contact_number">
                                    </div>
                                </div>
                            </div>
                             <hr>

                                <h4 class="card-title">Requirements </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">DTI</label>
                                        <input type="text" class="form-control" placeholder="DTI" name="dti">
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>BIR 2303</label>
                                        <input type="text" class="form-control" placeholder="BIR2303" name="bir">
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>TIN NUMBER</label>
                                        <input type="text" class="form-control" placeholder="TIN NUMBER" name="tin">
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                            <button type="submit" id='save_merchant' class="btn btn-info btn-fill pull-right"><span class='fa fa-plus-circle'></span> Save</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-image">
                        <img src="../assets/img/banner.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="../assets/img/business/logo.png" alt="..."  id="upload-demo">
                            </a> 
                            <input type="file" id="image" style="display: none;" required="">
                            <input type="hidden" name="user_id" id="user_id" required="">

                        </div>
                    </div>
                    <hr>
                    <div class="button-container mr-auto ml-auto">
                        <span  style="position: absolute;top: 210px;"><i class="nc-icon nc-cloud-upload-94" id="upload_image" style="color: black;font-size: 23px;font-weight: bolder;"></i> Select Image </span>
                          <button class="btn btn-info btn-fill pull-right btn-upload-image" disabled="" id="btn_upload">Upload Image</button>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#upload_image').on('click', function() {
            $('#image').trigger('click');
        });

        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });

        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 150,
                height: 150,
                type: 'square' //square
            },
            boundary: {
                width: 150,
                height: 150
            }
        });

        $('.btn-upload-image').on('click', function (ev) {
            var user_id = $("#user_id").val();
          resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
          }).then(function (img) {
            $.ajax({
              url: "../ajax/upload_image.php",
              type: "POST",
              data: {"image":img,"user_id":user_id},
              success: function (data) {
                success_update();
                   if(get_user == 'D'){
                         window.location.replace("index.php?page=driver");
                    }else{
                         window.location.replace("index.php?page=laundry");
                    }
              }
            });
          });
        });

        $("#AddDriverForm").submit(function(e){
        e.preventDefault();
        $("#save_merchant").prop("disabled", true);
        $("#save_merchant").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        var get_user = $("#get_user").val();
        if(get_user == 'D'){
            url = "addDriver";
        }else{
            url = "addMerchant";
        }
        $("#saveInfo").prop("disabled", true);
        $("#saveInfo").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
         $.ajax({
            url:"../ajax/"+url+".php",
            method:"POST",
            data:$(this).serialize(),
            success: function(data){
            if(data > 0){
                $("#user_id").val(data);
                $("#btn_upload").prop("disabled",false);
                success_add();
            }else if(data == 0){
                failed_query();
            }else if(data == 'E'){
                alert_custom("fa fa-exclamation","Unable to Add User","Please connect to internet, some of the details need internet connection.","warning");
            }else{
                alert_custom("fa fa-exclamation","Aw Snap!","Data already Exist","warning");
            }
            $("#save_merchant").prop("disabled", false);
            $("#save_merchant").html("<span class='fa fa-plus-circle'></span> Save ");
            }
          });
        })

    })
</script>