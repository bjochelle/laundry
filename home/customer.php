<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of Customer 
        </h4>
        </div>
        <div class="card-body table-full-width table-responsive">

            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                    <th>Name</th>
                    <th>Contact Number</th>
                    <th>Date Added</th>
                    <th>Action</th>

                </tr></thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      var status = 'C';
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"../ajax/datatables/dt_user.php",
          "dataSrc":"data",
          "data":{
              status:status
            }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"name"
          },
          {
            "data":"contact_number"
          },
          {
            "data":"date_added"
          },
          {
            "mRender": function(data,type,row){
              return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='" + row.id+ "' onclick='viewCustomer("+row.id+")'><span class='fa fa-eye'></span></button></center>";
            }
          }
        ]
      });
    }

  function viewCustomer(id){
        window.location.replace("index.php?page=viewCustomer&&id="+id);
  }
  
$(document).ready(function (){
  getData();
});
</script>