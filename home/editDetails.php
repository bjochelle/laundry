<?php

    include '../core/config.php';
    
    $id = $_GET['id'];
    $getStatus = getDetails($id,'tbl_user','status');
    if($getStatus == 'D'){
        $user_info = "Driver";
    }else{
        $user_info = "Laundry Merchant";
    }
?>
<style type="text/css">
    a:hover{
        text-decoration: none;
    }
</style>

<div class="content" style="width: 100%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-body">
                        <form id="editUserForm">
                            <input type='hidden' name='user_id' class="form-control" value="<?php echo $id;?> ">
                            <?php if($getStatus == 'D'){?>
                             
                                <h4 class="card-title">Vehicle Information </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                     <div class="form-group">
                                        <label>OR CR</label>

                                    <input type='text' name='or_cr' class="form-control" value="<?php echo getDetails($id,'tbl_driver','or_cr');?> ">
                                    </div>
                                </div>
                                <div class="col-md-8 px-1">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input type='text' name='motor_desc' class="form-control" value="<?php echo getDetails($id,'tbl_driver','motor_desc');?> ">
                                    </div>
                                </div>
                            </div>
                             <hr>

                                <h4 class="card-title">Requirements </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">Driver's License</label>
                                         <input type='text' name='drivers_license' class="form-control" value="<?php echo getDetails($id,'tbl_driver','drivers_license');?> ">
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Police Clearance</label>
                                        <input type='text' name='police_clearance' class="form-control" value="<?php echo getDetails($id,'tbl_driver','police_clearance');?> ">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>NBI Clearance</label>
                                        <input type='text' name='nbi_clearance' class="form-control" value="<?php echo getDetails($id,'tbl_driver','nbi_clearance');?> ">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Delivery Fee</label>
                                        <input type='number'  name='fee' class="form-control" value="<?php echo getDetails($id,'tbl_driver','fee');?>" readonly>
                                    </div>
                                </div>
                            </div>
                        <?php }else{?>
                                  <h4 class="card-title">Merchant Information </h4>
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="form-group">
                                        <label>Business Name</label>
                                        <input type='text' name='name' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','name');?> ">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Merchant Address</label>
                                        <input type='text' name='address' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','address');?> ">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                     <div class="form-group">
                                        <label>Email Address</label>
                                        <input type='email' name='email' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','email');?> ">
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input type='text' name='contact_number' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','contact_number');?> ">
                                    </div>
                                </div>
                            </div>
                             <hr>

                                <h4 class="card-title">Requirements </h4>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">DTI</label>
                                        <input type='text' name='dti' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','dti');?> ">
                                    </div>
                                </div>
                                 <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>BIR 2303</label>
                                        <input type='text' name='bir' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','bir');?> ">
                                    </div>
                                </div>
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                         <label for="exampleInputEmail1">TIN NO.</label>
                                        <input type='text' name='tin' class="form-control" value="<?php echo getDetails($id,'tbl_merchant','tin');?> ">
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                              <button type="submit" id='update_merch' class="btn btn-info btn-fill pull-right"><span class='fa fa-check-circle'></span> Update </button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-image">
                         <img src="../assets/img/banner.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="../assets/img/faces/<?php echo getDetails($id,'tbl_user','filename');?> " alt="...">
                                <h5 class="title"><?php echo ucwords(getDetails($id,'tbl_user','fname')." ".getDetails($id,'tbl_user','lname'));?></h5>
                            </a>
                            
                            <p class="description">
                                <?php echo getDetails($id,'tbl_user','un');?>
                            </p>
                        </div>
                        <p class="description text-center">
                            <?php echo getDetails($id,'tbl_user','email');?>
                            <br> <?php echo date('F d, Y',strtotime(getDetails($id,'tbl_user','bday')));?>
                            <br> <?php echo getDetails($id,'tbl_user','contact_number');?>
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#editUserForm").submit(function(e){
        e.preventDefault();
        $("#update_merch").prop("disabled", true);
        $("#update_merch").html("<span class='fa fa-spin fa-spinner'></span> Updating... ");
        var get_user = '<?php echo $getStatus;?>';
        if(get_user == 'D'){
            url = "updateDriver";
        }else{
            url = "updateMerchant";
        }
        
         $.ajax({
            url:"../ajax/"+url+".php",
            method:"POST",
            data:$(this).serialize(),
            success: function(data){
             
                if(data == 1){
                    success_update();
                }else if(data == 2){
                    alert_custom("fa fa-exclamation","Unable to update user details","Please connect to internet, some of the details need internet connection.","warning");
                }else{
                    failed_query();
                }
            $("#update_merch").prop("disabled", false);
            $("#update_merch").html("<span class='fa fa-check-circle'></span> Update");
            }
          });
        })

    })
</script>