<?php
    include '../core/config.php';
    $id = $_GET['id'];
?>
<style type="text/css">
    a:hover{
        text-decoration: none;
    }
</style>

<div class="content" style="width: 100%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-body">
                        <table class="table table-hover table-striped" id="table" style="width: 100%;">
                        <thead>
                            <tr><th>ID</th>
                            <th>Merchant</th>
                           <!--  <th>Merchant Address</th> -->
                            <th>Service Type</th>
                            <th>Status</th>
                            <th>Total Price</th>
                            <th>Date and Time</th>
                </tr></thead>
                <tbody>
                </tbody>
            </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-image">
                         <img src="../assets/img/banner.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="../assets/img/faces/<?php echo getDetails($id,'tbl_user','filename');?> " alt="...">
                                <h5 class="title"><?php echo ucwords(getDetails($id,'tbl_user','fname')." ".getDetails($id,'tbl_user','lname'));?></h5>
                            </a>
                            
                            <p class="description">
                                <?php echo getDetails($id,'tbl_user','un');?>
                            </p>
                        </div>
                        <p class="description text-center">
                            <?php echo getDetails($id,'tbl_user','email');?>
                            <br> <?php echo date('F d, Y',strtotime(getDetails($id,'tbl_user','bday')));?>
                            <br> <?php echo getDetails($id,'tbl_user','contact_number');?>
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  function getData(){
      var table = $('#table').DataTable();
      table.destroy();
      var id = <?php echo $id ;?>;
      $("#table").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"../ajax/datatables/dt_my_trans.php",
          "dataSrc":"data",
          "data":{
            id:id
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"merchant"
          },
          // {
          //   "data":"merch_addr"
          // },
          {
            "data":"service"
          },
          {
            "data":"status"
          },
          {
            "data":"price"
          }, 
          {
            "data":"date"
          },
        ]
      });
    }

 
$(document).ready(function (){
  getData();
});
</script>