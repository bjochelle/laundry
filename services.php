<?php include "header.php";?>
    <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="assets/img/hero/banner.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>Our Services</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Our Services</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!--? Categories Area Start -->
        <div class="categories-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Section Tittle -->
                        <div class="section-tittle text-center mb-80">
                            <span>Our Services</span>
                            <h2>What We Can Do For You</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat text-center mb-50">
                            <div class="cat-icon">
                                <span class="icon icon-bright-lightbulb"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Personalized Experience</a></h5>
                                <p> You can always reach us for your laundry concerns. Call us and we are happy to help.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat text-center mb-50">
                            <div class="cat-icon">
                                <span class="icon icon-tag-1"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Pricing</a></h5>
                                <p>The quality and cost of our service determine the price. Trust our pricing is reasonable and affordable.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat text-center mb-50">
                            <div class="cat-icon">
                                <span class="icon icon-reading-book"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Convenience</a></h5>
                                <p>We simplify the booking request. Simply book through our website for pick up request. Our system will notify you to confirm your request.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat text-center mb-50">
                            <div class="cat-icon">
                                <span class="icon icon-quality"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Quality</a></h5>
                                <p>  We take utmost care of your clothes, segregating the whites and colored clothes, as well as the dark colored ones to light. We use gentle yet effective cleaning detergents to ensure that it will not damage your clothes especially the delicates.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat text-center mb-50">
                            <div class="cat-icon">
                               <span class="icon icon-express-delivery"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Express Delivery</a></h5>
                                <p>We offer a rush laundry service if required. We can deliver within 8-24 hours. However, due to the current Covid-19 pandemic, an additional 2 days added to the time frame for the isolation of clothes before washing.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat text-center mb-50">
                            <div class="cat-icon">
                                <span class="icon icon-interface"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="#">Order Update</a></h5>
                                <p> We immediately notify you once the laundry is done and ready for delivery. Let us know your convenient time to accept the laundry so we can schedule accordingly.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Categories Area End -->
       
    </main>
<?php include "footer.php";?>