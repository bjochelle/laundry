<?php
    include "../core/config.php";
    session_start();
    $status = $_SESSION['status'];
    $id = getData($_SESSION['id'],'tbl_merchant','merchant_id','user_id');
    $sd = $_POST['sd'];
    $ed = $_POST['ed'];
    $id = $_POST['id'];
    $filter_by = $_POST['filter_by'];

    if($filter_by == 'L'){
        if($id == '-1'){
            $sub_info = 'All Merchant';
            $nstr="";
            $add_query = "";

        }else{
            $sub_info = 'Merchant Name : '. getData($id,'tbl_merchant','name','merchant_id');
            $add_query = "and merchant_id=".$id;

            $nstr = "";
            $nstr_array = array();
            $tin = str_replace(" ","",getData($id,'tbl_merchant','tin','merchant_id'));
            $str_array  = str_split($tin,4);

            foreach ($str_array as $key => $value) {
                $nstr.=$value."-";
            }
            $nstr = "<center><h5>TIN NO.: ".substr($nstr,0,-1)."</h5></center>";
        }
    }else{
        if($id == '-1'){
            $sub_info = 'All Driver';
            $add_query = "";
        }else{
            $sub_info = 'Driver Name : '. ucwords(getFullname($id));
            $add_query = "and driver_id=".$id;

        }
        $nstr="";
    }

 

    $count_transaction = mysql_num_rows(mysql_query("SELECT * from tbl_transaction where status='F' and DATE_FORMAT(date_added,'%Y-%m-%d')>='$sd' and DATE_FORMAT(date_added,'%Y-%m-%d')<='$ed' $add_query "));

?>
<div class="card strpied-tabled-with-hover">

        <div class="card-header ">
            <center><h4 class="card-title" style="text-align: center;">Sales Summary<br>From <?php echo date('F d, Y', strtotime($sd))." to ".date('F d, Y', strtotime($ed))."<br>".$sub_info;?> <h5>Income Sales</h5>

            </h4></center>
        
        </div>

        <div class="card-body table-full-width table-responsive">
            <div class="row">
                <div class="col-md-12">
                    <strong>Total Transaction : <?= $count_transaction;?>  </strong>
                    <?php 
                    if($filter_by == 'L' && $id != '-1'){?> 
                       <!--   <button type="submit" class="btn btn-success pull-right" onclick="sendEmail()" style="float: right;">Send Report to Email</button> -->
                    <?php }?>
                </div>
              
            
            </div>
            
            
            <table class="table table-hover table-striped" id="table" style="width: 100%;">
                <thead>
                    <tr><th>ID</th>
                    <th>Service Type</th>
                    <th>Category</th>
                    <th>Customer Name</th>
                    <th>Total Price</th>
                    <th>Date</th>

                </tr></thead>
                <tbody>
                    <?php 

                        if($id == '-1'){
                            $fetch = mysql_query("SELECT * From tbl_transaction where status='F' and DATE_FORMAT(date_added,'%Y-%m-%d')>='$sd' and DATE_FORMAT(date_added,'%Y-%m-%d')<='$ed'");
                        }else{
                            if($filter_by =='L'){
                                $fetch = mysql_query("SELECT * From tbl_transaction where status='F' and DATE_FORMAT(date_added,'%Y-%m-%d')>='$sd' and DATE_FORMAT(date_added,'%Y-%m-%d')<='$ed' and merchant_id='$id' ");
                            }else{
                                $fetch = mysql_query("SELECT * From tbl_transaction where status='F' and DATE_FORMAT(date_added,'%Y-%m-%d')>='$sd' and DATE_FORMAT(date_added,'%Y-%m-%d')<='$ed' and driver_id='$id'");
                            }
                        }

                        $count = 1;
                        $sum = 0;
                        while($row = mysql_fetch_array($fetch)){
                            $total = ($row['price']*$row['qty'])+$row['fee'];
                            $sum+=$total;?>
                         
                            <tr>
                                <td><?php echo $count++;?></td>
                                <td><?php echo getServiceName($row['service_id']);?></td>
                                <td><?php echo getData($row['service_id'],'tbl_services','category','service_id');?></td>
                                <td><?php echo getFullname($row['user_id']);?></td>
                                <td><?php echo "Php ".number_format(($total),2);?></td>
                                <td><?php echo date('F d, Y',strtotime($row['date_added']));?></td>
                            </tr>

                    <?php }?>
                </tbody>
                <tfoot>
                    <tr><td colspan="4" style="text-align: right;font-weight: bold;">Total</td>
                    <td colspan="2" style="font-weight: bold;"><?php echo "Php ".number_format(($sum),2);?></td>

                </tr>
                </tfoot>
            </table>
            <?=$nstr?>
        </div>
    </div>
