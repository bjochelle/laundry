<?php

include '../core/config.php';

session_start();
$merchant_id=getData($_SESSION['id'],'tbl_merchant','merchant_id','user_id');
date_default_timezone_set('Asia/Manila');
$date = date("Y-m-d");

$header['series'] = array();
$data['name'] = 'Sales :';
$data['colorByPoint'] = true;
$data['data'] = array();

$start = 1;
while($start<=3){
		$list = array();
		if($start == 1){
			$name='Basic Items';
			$st='B';
		}else if($start == 2){
			$name = 'Dry Cleaning';
			$st='D';
		}else{
			$name = 'Special Items';
			$st='S';
		}

		// $sales= mysql_fetch_array(mysql_query("SELECT sum(t.price*qty) FROM `tbl_transaction` as t,tbl_services as s where t.service_id=s.service_id and status ='F' and DATE_FORMAT(date_added, '%Y-%m-%d') =  '$date' and service_type='$st' and t.merchant_id='$merchant_id'"));

		$sales= mysql_fetch_array(mysql_query("SELECT sum(t.price*qty) FROM `tbl_transaction` as t,tbl_services as s where t.service_id=s.service_id and status ='F' and DATE_FORMAT(date_added, '%Y-%m-%d') =  '$date' and service_type='$st' "));

		if($sales[0] == 0 or $sales[0] == null or $sales[0] == ''){
			$total = 0;
		}else{	
			$total = ($sales[0] * 1);
		}
		$list['name'] = $name;
		$list['drilldown'] = $name;
		$list['y'] = $total;
		array_push($data['data'], $list);
		$start++;
}

$header['drilldown'] = array();

//for drilldown
$fetch = mysql_query("SELECT * FROM `tbl_services` group by  service_type");
while($row = mysql_fetch_array($fetch)){
	
	$header_drilldown['series'] = array();

	$st = $row['service_type'];
	if($st == 'B'){
		$name='Basic Items';
	}else if($st == 'D'){
		$name = 'Dry Cleaning';
	}else{
		$name = 'Special Items';
	}

	$data_drilldown['name'] = $name;
	$data_drilldown['id'] = $name;
	$data_drilldown['data'] = array();

	//for drilldown
	$fetch_cat = mysql_query("SELECT * FROM tbl_services where service_type='$st' ");
	if(mysql_num_rows($fetch_cat)==0){

		$list_drilldown = array();

		$list_drilldow = [$name,0];
		array_push($data_drilldown['data'], $list_drilldow);
	}else{
		while($cat_row = mysql_fetch_array($fetch_cat)){
			$category = $cat_row['category'];
			// $sales_st= mysql_fetch_array(mysql_query("SELECT t.price*qty FROM `tbl_transaction` as t,tbl_services as s where t.service_id=s.service_id and status ='F' and DATE_FORMAT(date_added, '%Y-%m-%d') =  '$date' and service_type='$st'  and category='$category' and t.merchant_id='$merchant_id' "));
			$sales_st= mysql_fetch_array(mysql_query("SELECT t.price*qty FROM `tbl_transaction` as t,tbl_services as s where t.service_id=s.service_id and status ='F' and DATE_FORMAT(date_added, '%Y-%m-%d') =  '$date' and service_type='$st'  and category='$category' "));
			$list_drilldown = array();

			if($sales_st[0] == 0 or $sales_st[0] == null or $sales_st[0] == ''){
				$total = 0;
			}else{	
				$total = ($sales_st[0] * 1);
			}
			$list_drilldow = [$category,$total];
			array_push($data_drilldown['data'], $list_drilldow);
		}
	}

array_push($header['drilldown'], $data_drilldown);
array_push($header_drilldown['series'], $data_drilldown);
}

array_push($header['series'], $data);
echo json_encode($header);
?>