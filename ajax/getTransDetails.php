<?php
include '../core/config.php';

if(isset($_POST["id"]) && isset($_POST["id"]) != ""){
	$id = $_POST['id'];
	$query = "SELECT * from tbl_transaction where trans_id='$id'";
	$result = mysql_query($query) or die(mysql_error());
	$response = array();
	
	if(mysql_num_rows($result) > 0){
		while ($row = mysql_fetch_assoc($result)) {
        $response = $row;
        $response['merch_name'] = ucwords(getData($row['merchant_id'],'tbl_merchant','name','merchant_id'));
        $response['merch_address'] = ucwords(getData($row['merchant_id'],'tbl_merchant','address','merchant_id'));
        $response['service_type'] = getServiceName($row['service_id']);
        $response['drivers_name'] = getFullname($row['driver_id']);
        $response['category'] = getData($row['service_id'],'tbl_services','category','service_id');
        $response['price'] = getData($row['service_id'],'tbl_services','price','service_id').'/'.getData($row['service_id'],'tbl_services','packaging','service_id');
        $response['total'] = ($row['price']*$row['qty'])+$row['fee'];
        $response['fee'] = $row['fee'];

        $response['sched_pick'] = date('F d,Y g:i a',strtotime($row['sched_date'].' '.$row['sched_time']));
        $response['sched_drop'] = date('F d,Y g:i a',strtotime($row['sched_drop_off']));

      $DL = mysql_fetch_array(mysql_query("SELECT date_added FROM tbl_track_transaction where trans_id='$id' and module='DL'"));
      $PL = mysql_fetch_array(mysql_query("SELECT date_added FROM tbl_track_transaction where trans_id='$id' and module='PL'"));
      $DC = mysql_fetch_array(mysql_query("SELECT date_added FROM tbl_track_transaction where trans_id='$id' and module='DC'"));
      $PC = mysql_fetch_array(mysql_query("SELECT date_added FROM tbl_track_transaction where trans_id='$id' and module='PC'"));

  		$response['DL'] = $DL[0];
  		$response['PL'] = $PL[0];
  		$response['DC'] = $DC[0];
  		$response['PC'] = $PC[0];
      }
	}else
    {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    echo json_encode($response);
}