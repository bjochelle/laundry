<?php include "header.php";?>
    <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="assets/img/hero/banner.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>About us</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">About</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!--? About Area Start -->
        <div class="about-low-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="about-caption mb-50">
                            <!-- Section Tittle -->
                            <div class="section-tittle mb-35">
                                <span>WHO WE ARE</span>
                                <h2>Safe Laundry  Solutions That Saves our Valuable Time!</h2>
                            </div>
                            <p>The Bacolod Laundry  provides full-service for your laundry needs. We are committed to providing quality laundry and dry-cleaning services by using safe and gentle laundry detergents and dry-cleaning products. Our team is dedicated to handling your clothes with care. We pick up and drop off your clothes right at your place of work or your home.</p>



                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <!-- about-img -->
                        <div class="about-img ">
                            <div class="about-font-img">
                                <img src="assets/img/gallery/g7.jpg" alt="" style=" width: 100%;">
                            </div>
                            <div class="about-back-img d-none d-lg-block">
                                <img src="assets/img/gallery/about1.png" alt="">
                            </div>
                        </div>
                    </div>

                     <!--? Testimonial Start -->
<!--                         <div class="testimonial-area section-bg" style="width: 100%;">
                            <div class="container">
                                <div class="row justify-content-between">
                                    <div class="col-lg-12">
                                        <div class="section-tittle section-tittle2 mb-25">
                                            <span>Why Choose Us</span>
                                        </div> 
                                        <div class="col-md-6" style="float: left;">
                                           <h1><span class="icon icon-bright-lightbulb"></span> Personalized Experience </h1>
                                          You can always reach us for your laundry concerns. Call us and we are happy to help.
                                        </div>

                                        <div class="col-md-6" style="float: left;">
                                            <h1><span class="icon icon-tag-1"></span> Pricing</h1>
                                           The quality and cost of our service determine the price. Trust our pricing is reasonable and affordable.
                                        </div>

                                        <div class="col-md-6" style="float: left;">
                                             <h1><span class="icon icon-reading-book"></span> Convenience</h1>
                                             We simplify the booking request. Simply book through our website for pick up request. Our system will notify you to confirm your request.
                                        </div>

                                        <div class="col-md-6" style="float: left;">
                                             <h1><span class="icon icon-quality"></span>Quality</h1>
                                            We take utmost care of your clothes, segregating the whites and colored clothes, as well as the dark colored ones to light. We use gentle yet effective cleaning detergents to ensure that it will not damage your clothes especially the delicates.
                                        </div>

                                        <div class="col-md-6" style="float: left;">
                                             <h1><span class="icon icon-express-delivery"></span>Express Delivery</h1>
                                             We offer a rush laundry service if required. We can deliver within 8-24 hours. However, due to the current Covid-19 pandemic, an additional 2 days added to the time frame for the isolation of clothes before washing.
                                        </div>

                                        <div class="col-md-6" style="float: left;">
                                             <h1><span class="icon icon-interface"></span>Order Update</h1>
                                             We immediately notify you once the laundry is done and ready for delivery. Let us know your convenient time to accept the laundry so we can schedule accordingly.
                                        </div>
                                           
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- Testimonial End -->
                </div>
            </div>
        </div>
        <!-- About Area End -->


    </main>
   <?php include "footer.php";?>