  <?php include "header.php";?>
  <style type="text/css">
       #list ul li{
            list-style: decimal;
            font-size: 20px;
      }
  </style>
    <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="assets/img/hero/banner.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>Be Our Partner</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Be our partner</a></li> 
                                        <li class="breadcrumb-item"><a href="#">Laundry</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!-- ================ contact section start ================= -->
        <section class="contact-section" style="padding: 75px 0 75px;">
            <div class="container">
              
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Get Started</h2>
                    </div>
                    <div class="col-lg-8">
                        <div class="row" style="margin-left: 40px;" id="list">
                            <ul>
                                <li>Visit our location and look for our representative</li>
                                <li>Register and submit your details</li>
                                <li>Add your latest services and it we'll populate to online storefront</li>
                                <li>Start providing services to your customers</li>
                            </ul>
                        </div>
                        <br>
                         <h2 class="contact-title">Merchant Requirements</h2>
                            <div class="row" style="margin-left: 40px;" id="list">
                            <ul>
                                <li>DTI *</li>
                                <li>BIR2303*</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Negros Occidental,Philippines.</h3>
                                <p>Bacolod 6100</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>+639 253 565 2365</h3>
                                <p>Mon to Sun 8am to 10pm</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>support@laundry.com</h3>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ contact section end ================= -->
    </main>
  <?php include "footer.php";?>