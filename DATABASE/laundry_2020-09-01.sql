# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: laundry
# Generation Time: 2020-09-01 05:43:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_driver
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_driver`;

CREATE TABLE `tbl_driver` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `motor_desc` text NOT NULL,
  `or_cr` varchar(225) NOT NULL,
  `police_clearance` varchar(225) NOT NULL,
  `drivers_license` varchar(225) NOT NULL,
  `nbi_clearance` varchar(225) NOT NULL,
  `filename` text NOT NULL,
  `fee` int(11) NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_driver` WRITE;
/*!40000 ALTER TABLE `tbl_driver` DISABLE KEYS */;

INSERT INTO `tbl_driver` (`driver_id`, `user_id`, `motor_desc`, `or_cr`, `police_clearance`, `drivers_license`, `nbi_clearance`, `filename`, `fee`)
VALUES
	(1,14,'222','222','222','222','222','',0),
	(2,24,'test description s ','12546 3 ','test-police s ','test-driver license s ','test-nbi s ','',0);

/*!40000 ALTER TABLE `tbl_driver` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_merchant
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_merchant`;

CREATE TABLE `tbl_merchant` (
  `merchant_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `dti` varchar(225) NOT NULL,
  `bir` varchar(225) NOT NULL,
  `location_lat` varchar(50) NOT NULL,
  `location_long` varchar(50) NOT NULL,
  PRIMARY KEY (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_merchant` WRITE;
/*!40000 ALTER TABLE `tbl_merchant` DISABLE KEYS */;

INSERT INTO `tbl_merchant` (`merchant_id`, `user_id`, `name`, `address`, `contact_number`, `email`, `dti`, `bir`, `location_lat`, `location_long`)
VALUES
	(2,17,'Laundry Pal Bacolod  ','Door 1004, Gloria Esteban Building,Gatuslao Street,corner Galo Street,Barangay 12, Bacolod City,6100 ','asdsa@gmail.com   ','dasd@gmail.com','   ','   ','10.671002','122.9481345'),
	(3,18,'qqq  ','qqq  ','111  ','qq@gmail.com','qqqq  ','qqq  ','',''),
	(4,23,'Brgy.Mandalagan, Bacolod city,6100','Brgy.Mandalagan, Bacolod city,6100','dasd@gmail.com','4234','asdsa','dasdsa','10.6855122','122.9747512');

/*!40000 ALTER TABLE `tbl_merchant` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_rating
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_rating`;

CREATE TABLE `tbl_rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_rating` WRITE;
/*!40000 ALTER TABLE `tbl_rating` DISABLE KEYS */;

INSERT INTO `tbl_rating` (`rating_id`, `trans_id`, `service_id`, `rating`, `comment`, `date_added`, `user_id`)
VALUES
	(5,5,8,3,'test','2020-08-21 16:11:08',1),
	(6,5,8,4,'test','2020-08-21 16:18:08',1);

/*!40000 ALTER TABLE `tbl_rating` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_services`;

CREATE TABLE `tbl_services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type` varchar(225) NOT NULL,
  `category` text NOT NULL,
  `description` text NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `packaging` varchar(225) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_services` WRITE;
/*!40000 ALTER TABLE `tbl_services` DISABLE KEYS */;

INSERT INTO `tbl_services` (`service_id`, `service_type`, `category`, `description`, `price`, `packaging`, `merchant_id`)
VALUES
	(4,'B','Hand Wash','dasda',123.00,'kg',2),
	(6,'S','Comforter','sadas',13.00,'kg',2),
	(7,'D','Dress','sdas',3.00,'piece(s)',2),
	(8,'B','Hand Wash','hand wash',100.00,'kg',2),
	(9,'B','Press/Iron','dsad',213.00,'piece(s)',3),
	(10,'D','Dress','fdsf',12.00,'piece(s)',3);

/*!40000 ALTER TABLE `tbl_services` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_track_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_track_transaction`;

CREATE TABLE `tbl_track_transaction` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_id` int(11) NOT NULL,
  `module` varchar(225) NOT NULL,
  `date_added` datetime NOT NULL,
  `read_status` int(1) NOT NULL,
  PRIMARY KEY (`track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_track_transaction` WRITE;
/*!40000 ALTER TABLE `tbl_track_transaction` DISABLE KEYS */;

INSERT INTO `tbl_track_transaction` (`track_id`, `trans_id`, `module`, `date_added`, `read_status`)
VALUES
	(3,4,'PC','2020-05-27 13:45:37',1),
	(10,4,'DL','2020-05-27 14:33:33',1),
	(11,4,'PL','2020-05-27 14:33:38',1),
	(14,4,'DC','2020-05-27 14:36:14',1),
	(15,1,'PC','2020-08-20 15:36:27',1),
	(16,1,'DL','2020-08-20 15:36:40',1),
	(17,1,'PL','2020-08-20 15:36:46',1),
	(18,1,'DC','2020-08-20 15:36:49',1),
	(19,3,'PC','2020-08-21 09:41:50',1),
	(20,7,'PC','2020-08-21 09:44:43',1),
	(21,7,'DL','2020-08-21 09:45:37',1),
	(22,13,'PC','2020-08-21 13:52:29',1),
	(23,3,'DL','2020-08-21 14:05:53',1),
	(24,3,'PL','2020-08-21 14:06:07',1),
	(25,3,'DC','2020-08-21 14:06:15',1),
	(26,13,'DL','2020-08-22 18:03:19',0),
	(27,13,'PL','2020-08-22 18:03:23',0),
	(29,13,'DC','2020-08-22 18:05:02',0),
	(30,14,'PC','2020-08-24 14:51:28',0),
	(31,14,'DL','2020-08-24 14:51:32',0),
	(32,14,'PL','2020-08-24 14:51:41',0);

/*!40000 ALTER TABLE `tbl_track_transaction` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_transaction`;

CREATE TABLE `tbl_transaction` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `note` text NOT NULL,
  `date_added` datetime NOT NULL,
  `sched_date` date NOT NULL,
  `sched_time` time NOT NULL,
  `sched_drop_off` datetime NOT NULL,
  `date_finish` datetime NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `qty` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_transaction` WRITE;
/*!40000 ALTER TABLE `tbl_transaction` DISABLE KEYS */;

INSERT INTO `tbl_transaction` (`trans_id`, `user_id`, `merchant_id`, `service_id`, `driver_id`, `status`, `note`, `date_added`, `sched_date`, `sched_time`, `sched_drop_off`, `date_finish`, `price`, `qty`, `fee`)
VALUES
	(1,1,2,7,14,'F','sample','2020-05-27 00:00:00','0000-00-00','00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0.000,10,0),
	(3,1,2,8,1,'F','','2020-08-22 00:00:00','0000-00-00','00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0.000,13,0),
	(4,1,2,6,14,'F','sada','2020-05-28 00:00:00','2020-08-19','08:00:00','2020-08-22 10:00:00','0000-00-00 00:00:00',13.000,12,0),
	(5,1,2,8,0,'F','note ','2020-08-21 00:00:00','0000-00-00','00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',123.000,1,0),
	(6,1,2,7,14,'C','','2020-08-20 00:00:00','0000-00-00','00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',3.000,0,0),
	(7,1,2,7,0,'C','14','2020-08-21 00:00:00','2020-08-22','00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',3.000,12,0),
	(12,1,3,10,0,'X','','2020-08-21 10:49:24','2020-08-21','11:48:00','2020-08-21 12:00:00','0000-00-00 00:00:00',12.000,0,0),
	(13,1,3,10,24,'F','','2020-08-21 13:51:27','2020-08-22','14:51:00','2020-08-24 14:51:00','2020-08-22 18:05:02',12.000,20,0),
	(14,1,3,9,24,'C','','2020-08-24 14:49:47','2020-08-25','14:49:00','2020-08-28 02:49:00','0000-00-00 00:00:00',213.000,1,0);

/*!40000 ALTER TABLE `tbl_transaction` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(225) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `bday` date NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `un` varchar(225) NOT NULL,
  `pw` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `date_added` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  `ishidden` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;

INSERT INTO `tbl_user` (`user_id`, `fname`, `lname`, `bday`, `contact_number`, `address`, `un`, `pw`, `status`, `date_added`, `email`, `filename`, `ishidden`)
VALUES
	(1,'Jihyo','Park','2020-05-22','421421421','tangub','q','q','C','2020-05-22 00:00:00','jihyo@gmail.com','1590469994.png',0),
	(2,'Ezra','tan','2020-05-22','5325','132','a','a','A','2020-05-22 00:00:00','sdas@gmail.com','1597973767.png',1),
	(3,'asdas','dsad','2020-05-27','1412','','das','dsa','C','2020-05-22 00:00:00','','',0),
	(4,'sadsa','das','2020-05-01','3423','','sdas','dsa','C','2020-05-22 00:00:00','','',0),
	(5,'das','dasda','2020-05-22','414','','1241','q','C','2020-05-22 18:58:27','','',0),
	(14,'q','wq','2020-05-01','34321','rweqq','qq','1234','D','2020-05-26 10:22:46','sdaq@gmail.com','1597909582.png',0),
	(17,'John','Doe','2020-05-26','42343','rewr','jd','12345','L','2020-05-26 01:11:20','dasd@GMAIL.com','1597909582.png',0),
	(18,'q','qqq','0000-00-00','21321312 ','dasd ','l','12345','L','2020-05-26 01:12:34','41234 ','1590643326.png',0),
	(23,'das','sad','2020-06-08','412412','sdas','dsad','12345','L','2020-06-08 14:44:32','dsadas@gmail.com','',0),
	(24,'Jiwon','Lee','1990-07-01','09500238475','Purok Roadstreet, Brgy.Tangub','jiwon_lee','12345','D','2020-08-21 09:26:45','jiwonlee@gmail.com','1597973208.png',0);

/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
