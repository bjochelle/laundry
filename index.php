<?php 
include "core/config.php";
include "header.php";
?>
<style type="text/css">
    .categories-area .single-cat{
        padding: 20px 20px;
    }
    .categories-area .single-cat::before{
        background: #B2EBF2;
    }
      #map {
        height: 600px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>

<mai>
    <!--? slider Area Start-->
    <div class="slider-area ">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-9 col-lg-9">
                            <div class="hero__caption">
                                <h1 style="background: #9b4e31;padding: 10px;">Safe & Reliable Laundry Solutions!</h1>
                            </div>
                            <!--Hero form -->
                            <!-- <form action="#" class="search-box">
                                <div class="input-form">
                                    <input type="text" placeholder="Your Tracking ID">
                                </div>
                                <div class="search-form">
                                    <a href="#">Track & Trace</a>
                                </div>	
                            </form>	
                            <div class="hero-pera">
                                <p>For order status inquiry</p>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider Area End-->
    <!--? our info Start -->
    <div class="our-info-area pt-70 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-support"></span>
                        </div>
                        <div class="info-caption">
                            <p>Call Us Anytime</p>
                            <span>+ (639) 910-123-4569</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-clock"></span>
                        </div>
                        <div class="info-caption">
                            <p>Sunday CLOSED</p>
                            <span>Mon - Sat 8:00 am - 5:00 pm</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-place"></span>
                        </div>
                        <div class="info-caption">
                            <p>Bacolod, 6100</p>
                            <span>Philippines, Negros Occidental</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- our info End -->

      <!--? our info Start -->
    <div class="our-info-area pt-70 pb-40">
        <div class="container">
                 <h2 style="text-align: center;">How It Works in 4 Easy Steps</h2>

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="icon icon-interface"></span>
                        </div>
                        <div class="info-caption">
                            <p>Step 1</p>
                            <span>Book thru our website</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="icon icon-delivery-truck"></span>
                        </div>
                        <div class="info-caption">
                            <p>Step 2</p>
                            <span>We pick up your clothes</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="icon icon-technology"></span>
                        </div>
                        <div class="info-caption">
                            <p>Step 3</p>
                            <span>We wash your clothes</span>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="icon icon-towel2"></span>
                        </div>
                        <div class="info-caption">
                            <p>Step 4</p>
                            <span>We deliver clean, folded clothes</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- our info End -->

 

            <!--? Categories Area Start -->
    <!--     <div class="categories-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Section Tittle 
                        <div class="section-tittle text-center mb-80">
                            <span>Our Services</span>
                            <h2>What We Can Do For You</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                 
        <?php 
        $fetch = mysql_query("SELECT * FROM tbl_services ORDER BY `tbl_services`.`price` ASC ");
        while($row = mysql_fetch_array($fetch)){
          if($row['service_type'] == 'B'){
            $service_type = 'Basic Items';
            $image = "washing";
          }else if($row['service_type'] == 'D'){
            $service_type = 'Dry Cleaning';
            $image = "dry_cleaning";
          }else{
            $service_type = 'Special Items';
            $image = "blanket";

          }
          ?>
             <div class="col-lg-3 col-md-6 col-sm-6" id="service_id<?php echo $row['service_id'];?>" onclick="addService(<?php echo $row['service_id'].",".$row['merchant_id'];?>)" style="    margin-bottom: 40px;">
               
              <div class="single-cat text-center ">
                      <h4 class="card-title" id="merch_name<?php echo $row['service_id'];?>"><?php echo ucwords(getData($row['merchant_id'],'tbl_merchant','name','merchant_id'));?></h4>
                      <p class="card-category" id="cat<?php echo $row['service_id'];?>"><?php echo $service_type." (".$row['category'];?>)</p>
                  <div class="card-body ">
                      <img src="assets/img/elements/<?php echo $image;?>.png" style="width: 100%;">
                     
                      <hr>
                      <div class="stats" style="color: #1f77d0;" id="price<?php echo $row['service_id'];?>">
                          <i class="fa fa-tag"></i> Php <?php echo $row['price']."/".$row['packaging'];?>
                      </div>
                  </div>
              </div>
          </div>
        <?php }?>  
                </div>
            </div>
        </div> -->
        <!-- Categories Area End -->

        <div class="our-info-area pt-70 pb-40">
            <div class="container">
                <h2 style="text-align: center;">LOCATIONS</h2>
            
                <?php 
                    if(checkdnsrr('php.net')){
                        echo '<div id="map" style="margin-top: 50px;"></div>';
                    }else{
                        echo "<center><h3 style='text-align: center;color: red' class='animated shake'> <span class='fa fa-exclamation'></span> Unable to load Google Map, Please Connect to Internet.<h3></center>";
                    }
                ?>
           
            </div>
        </div>
</main>
<?php include "footer.php";?>

<script type="text/javascript">

function initMap() {
    var locations = [<?php 
                        $getAlllocations = mysql_query("SELECT * FROM `tbl_merchant`");
                        while($fetchrow = mysql_fetch_array($getAlllocations)){
                            $response = array();
                            $response[] = "<strong>Location: </strong>".$fetchrow["address"]."<br><br>"."<strong>Business Name: </strong> ".$fetchrow["name"];
                            $response[] = $fetchrow["location_lat"];
                            $response[] = $fetchrow["location_long"];
                            echo json_encode($response).",";
                        }
                        ?>];
    //alert(locations);
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: new google.maps.LatLng(10.6840,122.9563),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
    }
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=initMap">
    </script>